#include "rioTile.h"

SDL_Texture* getTile(SDL_Renderer* renderer, SDL_Surface* tileSet, int x, int y, int tileSize, SDL_Color transparencyColor)
{
	// We create a SDL_Texture that we will return and that will contain the tile we extracted
	SDL_Texture* tile = NULL;

	// If everything is okay
	if (tileSet != NULL && // Tileset exists
		tileSize > 0 && // The size of one tile is positive
		0 <= x*tileSize && x*tileSize - tileSize < tileSet->w && // The coordinates of the tile are on the tileset
		0 <= y*tileSize && y*tileSize - tileSize < tileSet->h)
	{
		// We initialize the SDL_Texture correctly
		tile = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, tileSize, tileSize);
		// We convert the format of the surface to be sure that it has the same of our texture
		SDL_Surface* tileSetCorrectFormat = SDL_ConvertSurfaceFormat(tileSet, SDL_PIXELFORMAT_RGBA8888, 0);
		// We save the format that we are using
		SDL_PixelFormat* format = SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888);

		// We lock the surface to be sure that it is not modified while we're using it
		SDL_LockSurface(tileSetCorrectFormat);
		// We get the pixels of the surface in an array
		Uint32* pixelsRef = tileSetCorrectFormat->pixels;
		// This variable will be used to store the pixel information of the texture
		Uint32 pixels[tileSize*tileSize];
		// We set the color value which is transparent on the image
		Uint32 transparencyPixel = SDL_MapRGBA(format, transparencyColor.r, transparencyColor.g, transparencyColor.b,  transparencyColor.a);

		// We set the offsets of the tile from the tilseset
		int offsetX = x*tileSize;
		int offsetY = y*tileSize;
		// For each pixel
		int i, j;
		for (i = 0; i < tileSize; i++)
		{
			for (j = 0; j < tileSize; j++)
			{
				// If the current pixel is suppose to be transparent
				if (pixelsRef[(i + offsetY)*(tileSet->w) + (j + offsetX)] == transparencyPixel)
				{
					// We set it transparent
					pixels[i*tileSize + j] = SDL_MapRGBA(format, 0, 0, 0, 0);
				}
				// Otherwise
				else
				{
					// We get the correct value from the tileSet
					pixels[i*tileSize + j] = pixelsRef[(i + offsetY)*(tileSet->w) + (j + offsetX)];
				}
			}
		}
		// Once everythin gis finished
		// We unlock the surface
		SDL_UnlockSurface(tileSetCorrectFormat);
		// We update the texture
		SDL_UpdateTexture(tile, NULL, pixels, sizeof(Uint32)*tileSize);
		// We say that it uses transparency
		SDL_SetTextureBlendMode(tile, SDL_BLENDMODE_BLEND);
		// We free everything that was initialize
		SDL_FreeFormat(format);
		SDL_FreeSurface(tileSetCorrectFormat);
	}
	// Finaly, we return the texture
	return tile;
}

RIO_TileSetMap* createTileSetMap(SDL_Renderer* renderer, int tileSize, SDL_Color transparencyColor, char* tilesInfoPath)
{
	RIO_TileSetMap* tileSetStruct = NULL; // Use to store all the information of the tileset
	// If the arguments are well defined
	if (renderer != NULL && tileSize > 0)
	{
		// We allocate the memory to store the tileset
		tileSetStruct = (RIO_TileSetMap*) malloc(sizeof(RIO_TileSetMap));
		// We store the length of a tile in the structure
		tileSetStruct->tileSize = tileSize;

		// We set the number of tile info to 0
		tileSetStruct->nbTileInfo = 0;
		// We set the list of tiles info to NULL
		tileSetStruct->tilesInfo = NULL;
		// We load the tile info from the structure
		int error = loadTileInfo(tileSetStruct, tilesInfoPath, renderer, transparencyColor);
		// Depending on the error
		switch (error)
 		{
 			case RIO_ERROR_FILE:
 				printf("%sError%s: can't extract tile information from the file \"%s\"\n",
 				BOLDRED_COLOR_TER,
 				RESET_COLOR_TER,
 				tilesInfoPath);
 				break;
 			case RIO_ERROR_FIELD:
 				printf("%sError%s: one or more critical fields in the tile information file \"%s\" are incorrect\n",
 				BOLDRED_COLOR_TER,
 				RESET_COLOR_TER,
 				tilesInfoPath);
 				break;
 			case RIO_ERROR_NULLFILE:
 				printf("%sError%s: NULL path for the tile information file\n",
 				BOLDRED_COLOR_TER,
 				RESET_COLOR_TER);
 				break;
 		}
	}
	// Then, we return the structure
	return tileSetStruct;
}

void destructTileSetMap(RIO_TileSetMap** tileSet)
{
	// If the pointer exists
	if (tileSet != NULL)
	{
		// If what he points to exists
		if (*tileSet != NULL)
		{
			// For each tile info
			int line;
			for (line = 0; line < (*tileSet)->nbTileInfo; line++)
			{
				// If their is tiles info in the list
				if ((*tileSet)->tilesInfo != NULL)
				{
					// We free the info of the tile from the memory
					destructTileInfo(&(*tileSet)->tilesInfo[line]);
				}
			}
			// We free the list of tile info
			free((*tileSet)->tilesInfo);

			// We free the structure
			free(*tileSet);
			// We set the pointer to NULL to avoid problems
			*tileSet = NULL;
		}
	}
}

RIO_TileInfo* initTileInfo(char* name, char* pathTileSetImage, int widthTileSet, int heightTileSet, int tileSize, int id, bool isAutoTile, bool isSolid, SDL_Color transparencyColor, SDL_Renderer* renderer)
{
	// We allocate the memory to store the tile info
 	RIO_TileInfo* tileInfo = (RIO_TileInfo*) malloc(sizeof(RIO_TileInfo));
 	// We set the variables of the tile info
 	strcpy(tileInfo->name, name);
 	strcpy(tileInfo->tileSetPath, pathTileSetImage);
 	tileInfo->widthTileSet = widthTileSet;
 	tileInfo->heightTileSet = heightTileSet;
 	tileInfo->tileSize = tileSize;
 	tileInfo->id = id;
 	if (isAutoTile)
 	{
 		tileInfo->nbTexture = 16;
 	}
 	else
 	{
 		tileInfo->nbTexture = 1;
 	}
 	tileInfo->listTexture = (SDL_Texture**) malloc(sizeof(SDL_Texture*) * tileInfo->nbTexture);
 	int i;
 	for(i = 0; i < tileInfo->nbTexture; i++)
 	{
		SDL_Surface* tileSet_Surface = SDL_LoadBMP(pathTileSetImage);
 		tileInfo->listTexture[i] = getTile(
			renderer,
			tileSet_Surface,
			widthTileSet + i%4,
			heightTileSet + (int) floor(i/4),
			tileSize,
			transparencyColor);
		SDL_FreeSurface(tileSet_Surface);
 	}

 	tileInfo->isAutoTile = isAutoTile;
 	tileInfo->isSolid = isSolid;
 	// Then, we return the structure
 	return tileInfo;
}

void destructTileInfo(RIO_TileInfo** tileInfo)
{
	// If the pointer exists
	if (tileInfo != NULL)
	{
		// If what he points to exists
		if (*tileInfo != NULL)
		{
			int index;
			for(index = 0; index < (*tileInfo)->nbTexture; index++)
			{
				SDL_DestroyTexture((*tileInfo)->listTexture[index]);
			}
			free((*tileInfo)->listTexture);
			free(*tileInfo);
			*tileInfo = NULL;
		}
	}
}

int loadTileInfo(RIO_TileSetMap* tileSet, char* pathTileInfo, SDL_Renderer* renderer, SDL_Color transparencyColor)
{
	int error = RIO_ERROR_NONE; // use to keep track of the error
	if (pathTileInfo != NULL && renderer != NULL)
	{
    	// Read the given file
    	RIO_VarField* tileRootFields = loadFieldsFromPath(pathTileInfo);
    	// If the root is defined correctly
        if (tileRootFields != NULL &&												// Exists
            tileRootFields->hasSubFields && tileRootFields->subFields != NULL)		// has subfields
        {
        	// We update the number of tile info
			tileSet->nbTileInfo = tileRootFields->nbSubFields;
			// We allocate the memory for the tiles info
			tileSet->tilesInfo = (RIO_TileInfo**) malloc(sizeof(RIO_TileInfo*) * tileSet->nbTileInfo);
			// We initialize all the tiles info with dummy values
			int tileIndex;
			for (tileIndex = 0; tileIndex < tileSet->nbTileInfo; tileIndex++)
			{
				tileSet->tilesInfo[tileIndex] = NULL;
			}
			
			// For each subfields of the root fields (each tile)
			int i;
            for(i = 0; i < tileRootFields->nbSubFields; i++)
            {
            	RIO_VarField* currentTileField = tileRootFields->subFields[i];
                // Get the name of the tile
                char* tile_name = currentTileField->name;
                
                // Get the id of the tile
                int tile_id = -1;
                RIO_VarField* tileField_id = findField("id", currentTileField);
    			if (tileField_id != NULL)
        			tile_id = tileField_id->intValue;
        		else
        			error = RIO_ERROR_FIELD;
        			
        		// Get if the tile is solid or not
        		bool tile_isSolid = false;
                RIO_VarField* tileField_isSolid = findField("isSolid", currentTileField);
    			if (tileField_isSolid != NULL)
        			tile_isSolid = tileField_isSolid->boolValue;
        			
        		// Get all the infos about the tileset
        		char tile_path[256];
        		bool tile_isAutoTile = false;
        		int tile_x = 0;
        		int tile_y = 0;
        		RIO_VarField* tileField_tilset = findField("tileset", currentTileField);
    			if (tileField_tilset != NULL && tileField_tilset->hasSubFields && tileField_tilset->subFields != NULL)
    			{
    				RIO_VarField* tileField_path = findField("path", tileField_tilset);
    				if (tileField_path != NULL)
        				strcpy(tile_path, tileField_path->stringValue);
        			else
        				error = RIO_ERROR_FIELD;
        				
        			RIO_VarField* tileField_isAutoTile = findField("isAutoTile", tileField_tilset);
    				if (tileField_isAutoTile != NULL)
        				tile_isAutoTile = tileField_isAutoTile->boolValue;
        				
        			RIO_VarField* tileField_x = findField("x", tileField_tilset);
    				if (tileField_x != NULL)
        				tile_x = tileField_x->intValue;
        				
        			RIO_VarField* tileField_y = findField("y", tileField_tilset);
    				if (tileField_y != NULL)
        				tile_y = tileField_y->intValue;
        		}
                
                tileSet->tilesInfo[i] = initTileInfo(
						tile_name,
						tile_path,
						tile_x,
						tile_y,
						tileSet->tileSize,
						tile_id,
						tile_isAutoTile,
						tile_isSolid,
						transparencyColor,
						renderer);
            }
        }
        else
        	error = RIO_ERROR_FILE;
    }
    else
        error = RIO_ERROR_NULLFILE;
    return error;
}

RIO_TileSetEntity* createTileSetEntity(SDL_Renderer* renderer, int tileSize, SDL_Color transparencyColor, char* pathTileSetImage)
{
	RIO_TileSetEntity* tileSetStruct = NULL; // Use to store all the information of the tileset
	// If the arguments are well defined
	if (renderer != NULL && tileSize > 0)
	{
		//We create a surface based on the BMP tileset image
		SDL_Surface* tileSet_Surface = SDL_LoadBMP(pathTileSetImage);
		if (tileSet_Surface != NULL)
		{
			// We allocate the memory to store the tileset
			tileSetStruct = (RIO_TileSetEntity*) malloc(sizeof(RIO_TileSetEntity));
			// We calculate the height and the width of the tileset
			tileSetStruct->nbDirection = (int) (tileSet_Surface->h / tileSize);
			tileSetStruct->nbAnimation = (int) (tileSet_Surface->w / tileSize);
			// We store the length of a tile in the structure
			tileSetStruct->tileSize = tileSize;
			// We initialise the 2D matrice of SDL_Texture
			tileSetStruct->textureData = (SDL_Texture***) malloc(sizeof(SDL_Texture**) * tileSetStruct->nbDirection);
			int line, col;
			for (line = 0; line < tileSetStruct->nbDirection; line++)
			{
				tileSetStruct->textureData[line] = (SDL_Texture**) malloc(sizeof(SDL_Texture*) * tileSetStruct->nbAnimation);
				for (col = 0; col < tileSetStruct->nbAnimation; col++)
				{
					// With an extracted tile from a SDL_Surface tileset
					tileSetStruct->textureData[line][col] = getTile(renderer, tileSet_Surface, col, line, tileSize, transparencyColor);
				}
			}
			// We free the surface from the memory
			SDL_FreeSurface(tileSet_Surface);
		}
	}
	// Then, we return the structure
	return tileSetStruct;
}

void destructTileSetEntity(RIO_TileSetEntity** tileSet)
{
	// If the pointer exists
	if (tileSet != NULL)
	{
		// If what he points for exists
		if (*tileSet != NULL)
		{
			int line, col;
			// For each line
			for (line = 0; line < (*tileSet)->nbDirection; line++)
			{
				// For each column
				for (col = 0; col < (*tileSet)->nbAnimation; col++)
				{
					// We destroy the SDL_Texture
					SDL_DestroyTexture((*tileSet)->textureData[line][col]);
				}
				// Once we destroyed all the SDL_Texture, we free the line from the memory
				free((*tileSet)->textureData[line]);
			}
			// Once we freed all the lines, we free the textureData
			free((*tileSet)->textureData);

			// We free the structure
			free(*tileSet);
			// We set the pointer to NULL to avoid problems
			*tileSet = NULL;
		}
	}
}

