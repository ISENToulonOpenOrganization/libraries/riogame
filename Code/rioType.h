#ifndef RIOTYPE_H
	#define RIOTYPE_H

// error ID used when loading and saving
typedef enum RIO_Errors {RIO_ERROR_NONE = 0, RIO_ERROR_READ, RIO_ERROR_SIZE, RIO_ERROR_EMPTY, RIO_ERROR_FILE, RIO_ERROR_FIELD, RIO_ERROR_NULLFILE} RIO_Errors;

// Use to check the type of player
typedef enum RIO_TypePlayer {PLAYER1 = 0, PLAYER2, PLAYER3, PLAYER4, NB_PLAYER} RIO_TypePlayer;

#endif
