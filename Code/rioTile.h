// General header files
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// Header file
#include "SDL2/SDL.h"
#include "rioFunction.h"
#include "rioType.h"
#include "rioFile.h"

// This structure is used to store the information of a single tile in a tileset
typedef struct RIO_TileInfo
{
	char name[20];				// The name of the tile
	char tileSetPath[200];		// The path relative to the main of the BMP image used as a tileset
	int heightTileSet;			// The x coordinate of the first tile in the tileset
	int widthTileSet;			// The y coordinate of the first tile in the tileset
	int tileSize;				// The size of a tile
	int id;						// The unique ID of the tile
	int nbTexture;				// The number of texture used for this tile
	SDL_Texture** listTexture;	// All the texture of the tile
	
	bool isAutoTile;			// Used to know if this tile use auto tile
	bool isSolid;				// Used to know if an entity could go through the tile or not
} RIO_TileInfo;

// This structure is used to store the SDL_Texture of all tiles in a tileset of a map
typedef struct RIO_TileSetMap
{
	int tileSize;					// the length of a tile in the tileset
	int nbTileInfo;					// The number of tiles info
	RIO_TileInfo** tilesInfo;		// a list of pointer to RIO_TileInfo structures which contains the informations of the tiles
} RIO_TileSetMap;

// This structure is used to store the SDL_Texture of all tiles in a tileset
typedef struct RIO_TileSetEntity
{
	int nbDirection;				// The number of direction in the tileset
	int nbAnimation;				// The number of animation in the tileset
	int tileSize;					// the length of a tile in the tileset
	SDL_Texture*** textureData;		// All the texture of the tiles of the tileset (textureData[height][width])
} RIO_TileSetEntity;


/* @function
 * 		Use to extract a tile from a tileset
 *
 * @param
 *		renderer : 				the SDL_Renderer where the texture will be used
 *		tileSet : 				the SDL_Surface which contain the tileSet
 *		x : 					the x coordinate of the tile on the tileset (colomn)
 *		y : 					the y coordinate of the tile on the tileset (row).
 *								(0, 0) is the top left corner
 *		tileSize : 				the size of one tile on the tileset
 *		transparencyColor : 	the color which will be transparent in the tileset
 *
 * @return
 * 		Returns an SDL_Texture wich contain the tile which has been extracted
 */
SDL_Texture* getTile(SDL_Renderer* renderer, SDL_Surface* tileSet, int x, int y, int tileSize, SDL_Color transparencyColor);

/* @function
 * 		Use to convert a SDL_Surface tilest to a RIO_tileSetMap
 *
 * @param
 *		renderer : 				the SDL_Renderer where the texture will be used
 *		tileSet : 				the SDL_Surface which contain the tileSet
 *		tileSize : 				the size of one tile on the tileset
 *		transparencyColor : 	the color which will be transparent in the tileset
 *		tilesInfoPath : 		the path to the file which contains the tiles informations
 *
 * @return
 * 		Returns an RIO_tileSetMap wich contain the tileset
 */
RIO_TileSetMap* createTileSetMap(SDL_Renderer* renderer, int tileSize, SDL_Color transparencyColor, char* tilesInfoPath);

/* @function
 * 		Use to free from the memory a RIO_TileSetMap
 * 
 * @param
 *		tileSet : 	the tileSet to free
 */
void destructTileSetMap(RIO_TileSetMap** tileSet);

/* @function
 * 		Use to init a tileInfo
 *
 * @param
 *		name : 					the name of the tile info
 *		pathTileSetImage : 			the path to the tileSet BMP image
 *		widthTileSet : 			the x coordinate of the first tile in the tileset
 *		heightTileSet : 		the y coordinate of the first tile in the tileset
 *		tileSize : 				the size of the tile
 *		id : 					the id of the tile info
 *		isAutoTile : 			to know if the tile is using auto tile or not
 *		isSolid : 				to know if the entity could go through this tile or not
 *		transparencyColor : 	the color which will be transparent in the tileset
 *		renderer : 				the SDL_Renderer where the texture will be used
 *
 * @return
 * 		Returns an RIO_TileInfo wich contain information about a tile
 */
RIO_TileInfo* initTileInfo(char* name, char* pathTileSetImage, int widthTileSet, int heightTileSet, int tileSize, int id, bool isAutoTile, bool isSolid, SDL_Color transparencyColor, SDL_Renderer* renderer);

/* @function
 * 		Use to free from the memory a RIO_TileInfo
 * 
 * @param
 *		tileInfo : 	the tileInfo to free
 */
void destructTileInfo(RIO_TileInfo** tileInfo);

/* @function
 * 		Use to load the tile info of a tileset from a file
 *
 * @param
 *		tileSet : 				the tileset which will use the tile info
 *		pathTileInfo : 			the path of the file where the tile info is stored
 *		renderer : 				a renderer use to extract the tileset texxtures
 *		transparencyColor : 	the color use as transparency in the tileset textures
 *
 * @return
 *		return an error ID depending on what happend
 */
int loadTileInfo(RIO_TileSetMap* tileSet, char* pathTileInfo, SDL_Renderer* renderer, SDL_Color transparencyColor);

/* @function
 * 		Use to convert a SDL_Surface tilest to a RIO_TileSetEntity
 *
 * @param
 *		renderer : 				the SDL_Renderer where the texture will be used
 *		tileSize : 				the size of one tile on the tileset
 *		transparencyColor : 	the color which will be transparent in the tileset
 *		pathTileSetImage : 		the path to the BMP tileset image
 *
 * @return
 * 		Returns an RIO_TileSetEntity wich contain the tileset
 */
RIO_TileSetEntity* createTileSetEntity(SDL_Renderer* renderer, int tileSize, SDL_Color transparencyColor, char* pathTileSetImage);

/* @function
 * 		Use to free from the memory a RIO_TileSetEntity
 * 
 * @param
 *		tileSet : 	the tileSet to free
 */
void destructTileSetEntity(RIO_TileSetEntity** tileSet);


