#include "rioPathfinding.h"

node* initNode(int x, int y, int cost, int heuristic)
{
	node* newNode = malloc(sizeof(node));
	newNode->x = x;
	newNode->y = y;
	newNode->cost = cost;
	newNode->heuristic = heuristic;
	newNode->linkedNode = NULL;
	return newNode;
}

void destructNodes(node** frontNode)
{
	if (frontNode != NULL)
	{
		if (*frontNode != NULL)
		{
			while((*frontNode)->linkedNode != NULL)
			{
				free(popNode(frontNode));
			}
		}
	}
}

void setHeuristic(node* nodeToSet, node* endNode)
{
	nodeToSet->heuristic = nodeToSet->cost + distNodes(endNode, nodeToSet);
}

node* popNode(node** frontNode)
{
	node* popedNode = *frontNode;
	if(frontNode != NULL)
	{
		*frontNode = (*frontNode)->linkedNode;
	}
	popedNode->linkedNode = NULL;
	return popedNode;
}

void rmvNode(node** frontNode, node* nodeToRemove)
{
	if(frontNode != NULL)
	{
		if (*frontNode != NULL && nodeToRemove != NULL)
		{
			node* cursor = *frontNode;
			if(cursor == nodeToRemove)
			{
				*frontNode = cursor->linkedNode;
				nodeToRemove->linkedNode = NULL;
			}
			else
			{
				while(cursor->linkedNode != NULL)
				{
					if(cursor->linkedNode == nodeToRemove)
					{
						cursor->linkedNode = cursor->linkedNode->linkedNode;
						nodeToRemove->linkedNode = NULL;
						break;
					}
					cursor = cursor->linkedNode;
				}
			}
		}
	}
}

node* cpyNode(node* nodeToCpy)
{
	node* newNode = NULL;
	if (nodeToCpy != NULL)
	{
		newNode = initNode(nodeToCpy->x, nodeToCpy->y, nodeToCpy->cost, nodeToCpy->heuristic);
	}
	return newNode;
}

void insertFrontNode(node** frontNode, node* newNode)
{
	if(newNode != NULL)
	{
		newNode->linkedNode = *frontNode;
		*frontNode = newNode;
	}
}

void viewNodes(node** frontNode, SDL_Renderer* renderer, RIO_Camera* camera, SDL_Color col, int tileSize)
{
	if (tileSize > 2)
	{
		node* temp = *frontNode;
		SDL_SetRenderDrawColor(renderer, col.r, col.g, col.b, col.a);
		while(temp != NULL)
		{
			SDL_Rect tileRect = {temp->x + 3, temp->y + 3, tileSize-6, tileSize-6};
			// We update the rectangle with the camera
			updateTextureRectWithCamera(camera, &tileRect);
			SDL_RenderFillRect(renderer, &tileRect);
			temp = temp->linkedNode;
		}
	}
}

int nodeCmp(node* n1, node* n2)
{
	if (n1->heuristic < n2->heuristic)
	{
		return 1;
	}
	else if (n1->heuristic == n2->heuristic)
	{
		return 0;
	}
	else
	{
		return -1;
	}
}

int distNodes(node* n1, node* n2)
{
	return pow((n1->x - n2->x), 2) + pow((n1->y - n2->y), 2);
}

bool isCoordInSet(node** frontNode, int x, int y)
{
	node* temp = *frontNode;
	while(temp != NULL)
	{
		if(temp->x == x && temp->y == y)
		{
			return true;
		}
		temp = temp->linkedNode;
	}
	return false;
}

bool isNodeInSet(node** frontNode, node* nodeToCheck)
{
	if (frontNode != NULL)
	{
		if (*frontNode != NULL)
		{
			node* temp = *frontNode;
			while(temp != NULL)
			{
				if(temp == nodeToCheck)
				{
					return true;
				}
				temp = temp->linkedNode;
			}
		}
	}
	return false;
}

bool isNextTo(node* nodeToCheck, int x, int y)
{
	int xdist = abs(nodeToCheck->x - x);
	int ydist = abs(nodeToCheck->y - y);
	if (xdist + ydist == 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

node* getLastNode(node** frontNode)
{
	node* lastNode = *frontNode;
	if (frontNode != NULL)
	{
		if (*frontNode != NULL)
		{
			while(lastNode->linkedNode != NULL)
			{
				lastNode = lastNode->linkedNode;
			}
		}
	}
	return lastNode;
}

node* getLowestNode(node** openSet)
{
	node* lowestNode = NULL;
	if (openSet != NULL)
	{
		if (*openSet != NULL)
		{
			lowestNode = *openSet;
			node* temp = *openSet;
			while(temp != NULL)
			{
				if(temp->heuristic < lowestNode->heuristic)
				{
					lowestNode = temp;
				}
				temp = temp->linkedNode;
			}
		}
	}
	return lowestNode;
}

node* getPath(node** closedSet, node* endNode)
{
	node* path = NULL;
	if (closedSet != NULL)
	{
		if (*closedSet != NULL && endNode != NULL)
		{
			node* cursor = *closedSet;
			node* temp = endNode;
			int currentCost = 0;
			while(cursor != NULL)
			{
				if(cursor->x == temp->x && cursor->y == temp->y)
				{
					temp = cursor;
					currentCost = temp->cost;
					break;
				}
				cursor = cursor->linkedNode;
			}
			insertFrontNode(&path, cpyNode(temp));

			while(currentCost > 0)
			{
				cursor = *closedSet;
				while(cursor != NULL)
				{
					if(cursor->cost == currentCost - 1 && isNextTo(cursor, temp->x, temp->y))
					{
						temp = cursor;
						currentCost--;
						break;
					}
					cursor = cursor->linkedNode;
				}
				insertFrontNode(&path, cpyNode(temp));
			}
		}
	}
	return path;
}

void addNeighbors(node** openSet, node** closedSet, node* currentNode, node* endNode, RIO_Map* map)
{
	int x = currentNode->x;
	int y = currentNode->y;
	node* temp;
	
	if (x-1 >= 0 && !isCoordInSet(closedSet, x-1, y) && !isCoordInSet(openSet, x-1, y))
	{
		bool isValid = false;
		int i;
		for(i = 0; i < map->tileSet->nbTileInfo; i++)
		{
			if (map->mapData[x-1][y] == map->tileSet->tilesInfo[i]->id && !map->tileSet->tilesInfo[i]->isSolid)
			{
				isValid = true;
			}
		}
		if (isValid)
		{
			temp = initNode(x-1, y, currentNode->cost + 1, 0);
			setHeuristic(temp, endNode);
			insertFrontNode(openSet, temp);
		}
	}
	if (x+1 < map->width && !isCoordInSet(closedSet, x+1, y) && !isCoordInSet(openSet, x+1, y))
	{
		bool isValid = false;
		int i;
		for(i = 0; i < map->tileSet->nbTileInfo; i++)
		{
			if (map->mapData[x+1][y] == map->tileSet->tilesInfo[i]->id && !map->tileSet->tilesInfo[i]->isSolid)
			{
				isValid = true;
			}
		}
		if (isValid)
		{
			temp = initNode(x+1, y, currentNode->cost + 1, 0);
			setHeuristic(temp, endNode);
			insertFrontNode(openSet, temp);
		}
	}
	if (y-1 >= 0 && !isCoordInSet(closedSet, x, y-1) && !isCoordInSet(openSet, x, y-1))
	{
		bool isValid = false;
		int i;
		for(i = 0; i < map->tileSet->nbTileInfo; i++)
		{
			if (map->mapData[x][y-1] == map->tileSet->tilesInfo[i]->id && !map->tileSet->tilesInfo[i]->isSolid)
			{
				isValid = true;
			}
		}
		if (isValid)
		{
			temp = initNode(x, y-1, currentNode->cost + 1, 0);
			setHeuristic(temp, endNode);
			insertFrontNode(openSet, temp);
		}
	}
	if (y+1 < map->height && !isCoordInSet(closedSet, x, y+1) && !isCoordInSet(openSet, x, y+1))
	{
		bool isValid = false;
		int i;
		for(i = 0; i < map->tileSet->nbTileInfo; i++)
		{
			if (map->mapData[x][y+1] == map->tileSet->tilesInfo[i]->id && !map->tileSet->tilesInfo[i]->isSolid)
			{
				isValid = true;
			}
		}
		if (isValid)
		{
			temp = initNode(x, y+1, currentNode->cost + 1, 0);
			setHeuristic(temp, endNode);
			insertFrontNode(openSet, temp);
		}
	}
}

node* AStar(node** openSet, node** closedSet, node* startNode, node* endNode, RIO_Map* map)
{
	if (startNode != NULL && endNode != NULL)
	{
		if(*openSet == NULL)
		{
			return startNode;
		}

		node* lowestNode = getLowestNode(openSet);
		rmvNode(openSet, lowestNode);
		insertFrontNode(closedSet, lowestNode);
		if (lowestNode->x == endNode->x && lowestNode->y == endNode->y)
		{
			node* path = getPath(closedSet, endNode);
			if (path == NULL)
				return startNode;
			else
				return path;
		}
		else
		{
			addNeighbors(openSet, closedSet, lowestNode, endNode, map);
			return NULL;
		}
	}
	return startNode;
}

void updatePathToCoordinate(node** frontNode, int tileSize)
{
	if (frontNode != NULL)
	{
		if (*frontNode != NULL)
		{
			node* temp = *frontNode;
			while(temp != NULL)
			{
				temp->x *= tileSize;
				temp->y *= tileSize;
				temp = temp->linkedNode;
			}
		}
	}
}

node* getNodeAfter(node** path, node* indexNode)
{
	if (path != NULL)
	{
		if (*path != NULL)
		{
			if (indexNode == NULL)
			{
				return *path;
			}
			else
			{
				node* temp = *path;
				while(temp != NULL)
				{
					if (temp == indexNode)
					{
						return temp->linkedNode;
					}
					temp = temp->linkedNode;
				}
			}
		}
	}
	return NULL;
}
node* getNodeBefore(node** path, node* indexNode)
{
	if (path != NULL)
	{
		if (*path != NULL)
		{
			if (indexNode == NULL)
			{
				return *path;
			}
			else
			{
				node* temp = *path;
				while(temp != NULL)
				{
					if (temp->linkedNode == indexNode)
					{
						return temp;
					}
					temp = temp->linkedNode;
				}
			}
		}
	}
	return NULL;
}

node* getClosetNodeToCoord(node** path, int x, int y)
{
	node* closestNode = NULL;
	if (path != NULL)
	{
		if (*path != NULL)
		{
			node* temp = *path;
			int bestDist = -1;
			closestNode = NULL;
			while(temp != NULL)
			{
				int dist = pow(temp->x - x, 2) + pow(temp->y - y, 2);
				if (bestDist < 0 || dist < bestDist)
				{
					closestNode = temp;
					bestDist = dist;
				}
				temp = temp->linkedNode;
			}
		}
	}
	return closestNode;
}

node* getReversePath(node** path)
{
	node* newPath = NULL;
	if (path != NULL)
	{
		if (*path != NULL)
		{
			node* index = *path;
			while(index != NULL)
			{
				node* temp = newPath;
				newPath = cpyNode(index);
				newPath->linkedNode = temp;
				index = index->linkedNode;
			}
		}
	}
	return newPath;
}

