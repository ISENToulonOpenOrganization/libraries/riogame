
// Header file
#include "rioTile.h"
#include "rioDisplay.h"

// This structure is use to store the data of a map
typedef struct RIO_Map
{
	int height;					// The height of the map in tiles
	int width;					// The width of the map in tiles
	
	RIO_TileSetMap* tileSet;	// The tileset used to show the map (the size of a tile is use as collision box for now)
	
	int** mapData;				// The data of the map, containing numbers wich indicates the type of tile (mapData[width][height])

} RIO_Map;

/* @function
 * 		Use to initialize a map structure. Warning, at this point the map data is NULL
 *
 * @param
 *		tileSet : 	the tileset use by the map when it is shown
 *					be sure to have an extern variable to store the tileset because it will not be free with destructMap()
 *		path : 		the path from wich we want to load the map. Put NULL if their is none
 *
 * @return
 * 		Returns a RIO_Map structure
 */
RIO_Map* initMap(RIO_TileSetMap* tileSet, char* path);

/* @function
 * 		Free a map from the memory. Warning, this function will not free the tileset from the memory
 *
 * @param
 *		map : 	the map we will free from the memory
 */
void destructMap(RIO_Map** map);

/* @function
 * 		Use to show a map in a renderer
 *
 * @param
 *		map : 		the map we want to show
 *		renderer : 	the renderer use to show the entity
 *		camera : 	the camera that will show the environment. If their is none, put NULL
 */
void showMap(RIO_Map* map, SDL_Renderer* renderer, RIO_Camera* camera);

/* @function
 * 		Use to load the data of a map from a file
 *
 * @param
 *		map : 	the map where we will load the data in
 *		path : 	the path of the file where the map data is stored
 *
 * @return
 *		return an error ID depending on what happend
 */
int loadMap(RIO_Map* map, char* path);

