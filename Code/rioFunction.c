#include "rioFunction.h"

RIO_Shape* initShape(int nbPoint)
{
	RIO_Shape* shape = (RIO_Shape*) malloc(sizeof(RIO_Shape));
	shape->nbPoint = nbPoint;
	shape->listPoint = (RIO_Point*) malloc(sizeof(RIO_Point)*nbPoint);
	int i;
	for(i = 0; i < nbPoint; i++)
	{
		shape->listPoint[i].x = 0;
		shape->listPoint[i].y = 0;
	}
	return shape;
}

void destructShape(RIO_Shape** shape)
{
	if (shape != NULL)
	{
		if (*shape !=NULL)
		{
			free((*shape)->listPoint);
			free(*shape);
			*shape = NULL;
		}
	}
}

float nmap(float value, float vmin, float vmax, float min, float max)
{
	// Original lenght
	float vlen = vmax - vmin;
	// Futur lenght
	float len = max - min;
	// Mapping the value
	float mapValue = (value-vmin)/vlen;
	// Return the new value
	return min + mapValue * len;
}

int sign(float value)
{
	// Return 1, -1 or 0 depending on the sign
	return (value > 0)? 1 :
		((value < 0)? -1 : 0);
}

float* cpyList(float* list, int size)
{
	// We initialize the new list
	float* newList = malloc(sizeof(float)*size);
	// For each value in the list
	int i;
	for(i = 0; i < size; i++)
	{
		// We copy it to the new list
		newList[i] = list[i];
	}
	// Then, we return the new list
	return newList;
}

void destructMatrice2D(void** matrice, int size)
{
	// If the matrice is not NULL
	if (matrice != NULL)
	{
		// For each row of the matrice
		int i;
		for(i = 0; i < size; i++)
		{
			// If the line in the matrice is not NULL
			if (matrice[i] != NULL)
			{
				// We free it from the memory
				free(matrice[i]);
			}
		}
		// Then we free the matrice
		free(matrice);
	}
}

void shuffleList(float* list, int size)
{
	float temp; // use to store temporarly a value of a list which will be move
	// Both these indexes will be use to invert the position of two values in the list
	int index1;
	int index2;
	// We do a fixed number of index inversion
	int i;
	for(i = 0; i < size; i++)
	{
		// We choose two random index
		index1 = rand()%size;
		index2 = rand()%size;
		// We store the value of the first index in the temporary variable
		temp = list[index1];
		// We store the value of the second index in the first idnex
		list[index1] = list[index2];
		// We put the first value back to the second index
		list[index2] = temp;
	}
}

RIO_Point* getCrossingPoint(RIO_Line* l1, RIO_Line* l2)
{
    RIO_Point* point = NULL;
    if (l1 != NULL && l2 != NULL && l1->angle != l2->angle)
    {
        point = (RIO_Point*) malloc(sizeof(RIO_Point));
        // Mathemagic give us the point
        point->y =   ( l1->y * l1->x * tan(l2->angle) - l2->y * l2->x * tan(l1->angle) ) /
                    ( l1->x * tan(l2->angle) - l2->x * tan(l1->angle) );
        point->x = l2->x * (point->y - l2->y) / tan(l2->angle);
    }
    return point;
}
