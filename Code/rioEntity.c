#include "rioEntity.h"

char* TYPE_ENTITY_STRING[NB_TYPES] = {"NOTYPE", "PLAYER", "ENEMY", "NEUTRAL"};
char* STATE_ENTITY_STRING[NB_STATES] = {"IDLE", "MOVING", "ATTACKING", "KNOCKBACK", "DEAD"};
char* DIRECTION_ENTITY_STRING[NB_DIRECTIONS] = {"up", "right", "down", "left"};

RIO_Entity* initEntity(RIO_TileSetEntity* idleTileSet, float x, float y, float speedAnimation, float maxSpeed, float repelSpeed, float accelerationFactor, RIO_TypeEntity type)
{
 	// We allocate the memory to store the entity
 	RIO_Entity* entity = (RIO_Entity*) malloc(sizeof(RIO_Entity));
 	// We set all the basic variables
 	entity->maxHealth = 10;
	entity->currentHealth = 10;
	entity->damage = 2;
 	entity->x = x;
 	entity->y = y;
 	entity->speedVector[0] = 0;
 	entity->speedVector[1] = 0;
 	entity->maxSpeed = maxSpeed;
 	entity->direction[0] = 0;
 	entity->direction[1] = 0;

 	entity->accelerationFactor = accelerationFactor;

 	entity->repelSpeed = repelSpeed;
	entity->isSolid = false;
	entity->isImmovable = false;
 	// We set a default collision box
 	entity->collisionBox.x = 0;
 	entity->collisionBox.y = 0;
 	entity->collisionBox.w = 0;
	entity->collisionBox.h = 0;
 	if (idleTileSet != NULL)
 	{
	 	entity->collisionBox.w = idleTileSet->tileSize;
	 	entity->collisionBox.h = idleTileSet->tileSize;
 	}
 	entity->attackCollisionBox = NULL;
 	entity->attackCollisionIsActive = false;
 	entity->friendlyFire = false;

 	// We set the animation variables
 	entity->indexAnimation = 2;
 	entity->frameAnimation = 0;
 	entity->speedAnimation = speedAnimation;
 	entity->animationClock = 0;

 	// We set the tilesets
 	int i;
 	for (i = 0; i < NB_STATES; i++)
 	{
 		entity->tileSet[i] = NULL;
 		entity->offsets[i][0] = NULL;
 		entity->offsets[i][1] = NULL;
 	}
 	entity->tileSet[IDLE] = idleTileSet;

 	// We set some information variables
 	entity->type = type;
 	entity->state = IDLE;
 	entity->stateIsLock = false;

 	entity->pathToFollow = NULL;
 	entity->nextNodeInPath = NULL;
 	entity->pathType = ONE_WAY;
 	// Then, we return the entity
 	return entity;
}

RIO_ListEntity* initListEntity(int size)
{
	// We allocate the memory for the structure
	RIO_ListEntity* listEntity = (RIO_ListEntity*) malloc(sizeof(RIO_ListEntity));
	// We set the size of the list
	listEntity->size = size;
	// We allocate the memory for list of entities
	listEntity->entities = (RIO_Entity**) malloc(sizeof(RIO_Entity*) * size);
	// For each entities in the list
	int i;
	for (i = 0; i < size; i++)
	{
		// We set it to NULL
		listEntity->entities[i] = NULL;
	}
	// Then, we return the structure
	return listEntity;
}

void destructEntity(RIO_Entity** entity)
{
	// If the pointer exists
 	if (entity != NULL)
 	{
 		// If what he points for exists
 		if (*entity != NULL)
 		{
 			// We free the attackCollisionBox
 			free((*entity)->attackCollisionBox);
 			// We free the tilesets from the memory
 			int i;
 			for (i = 0; i < NB_STATES; i++)
 			{
 				destructTileSetEntity(&(*entity)->tileSet[i]);
 				free((*entity)->offsets[i][0]);
 				free((*entity)->offsets[i][1]);
 			}
 			// Free the entity from the memory
 			free(*entity);
 			*entity = NULL;
 		}
 	}
}

void destructListEntity(RIO_ListEntity** listEntity)
{
	// If the pointer exists
 	if (listEntity != NULL)
 	{
 		// If what he points for exists
 		if (*listEntity != NULL)
 		{
 			// For each entity in the list
 			int i;
 			for(i = 0; i < (*listEntity)->size; i++)
 			{
 				// We destruct the entity
 				destructEntity(&(*listEntity)->entities[i]);
 			}
 			// We free the list of entities
 			free((*listEntity)->entities);
 			// We free the structure
 			free(*listEntity);
 			*listEntity = NULL;
 		}
 	}
}

void updateCollisionBox(RIO_Entity* entity, float xOffset, float yOffset, float width, float height)
{
	// If the entity exists
	if (entity != NULL)
	{
		// We update the collision box with the new values
		entity->collisionBox.x = xOffset;
		entity->collisionBox.y = yOffset;
		entity->collisionBox.w = width;
		entity->collisionBox.h = height;
	}
}

int loadEntity(RIO_Entity** entity, char* pathEntityInfo, SDL_Renderer* renderer, SDL_Color transparencyColor)
{
    int error = RIO_ERROR_NONE; // use to keep track of the error
	if (pathEntityInfo != NULL && renderer != NULL)
	{
    	// Init basic entity if it's NULL
		if (*entity == NULL)
		{
			*entity = initEntity(NULL, 0, 0, 1, 5, 0, 1, ENEMY);
		}
		// Read the given file
    	RIO_VarField* entityRootFields = loadFieldsFromPath(pathEntityInfo);
	
    	//---- Base info
    	RIO_VarField* entity_type = findField("type", entityRootFields);
    	if (entity_type != NULL)
        	(*entity)->type = typeEntity_StringToEnum(entity_type->stringValue);
	
		RIO_VarField* entity_maxHealth = findField("maxHealth", entityRootFields);
    	if (entity_maxHealth != NULL)
    	{
        	(*entity)->maxHealth = entity_maxHealth->intValue;
        	(*entity)->currentHealth = (*entity)->maxHealth;
        }
        
        RIO_VarField* entity_damage = findField("damage", entityRootFields);
    	if (entity_damage != NULL)
        	(*entity)->damage = entity_damage->intValue;
        	
        RIO_VarField* entity_friendlyFire = findField("friendlyFire", entityRootFields);
    	if (entity_friendlyFire != NULL)
        	(*entity)->friendlyFire = entity_friendlyFire->boolValue;
        	
        RIO_VarField* entity_coordinate = findField("coordinate", entityRootFields);
    	if (entity_coordinate != NULL && entity_coordinate->hasSubFields && entity_coordinate->subFields != NULL)
    	{
    		RIO_VarField* entity_x = findField("x", entity_coordinate);
    		if (entity_x != NULL)
        		(*entity)->x = entity_x->floatValue;
        	RIO_VarField* entity_y = findField("y", entity_coordinate);
    		if (entity_y != NULL)
        		(*entity)->y = entity_y->floatValue;
        }
		
    	RIO_VarField* entity_maxSpeed = findField("maxSpeed", entityRootFields);
    	if (entity_maxSpeed != NULL)
        	(*entity)->maxSpeed = entity_maxSpeed->floatValue;
	
    	RIO_VarField* entity_accelerationFactor = findField("accelerationFactor", entityRootFields);
    	if (entity_accelerationFactor != NULL)
        	(*entity)->accelerationFactor = entity_accelerationFactor->floatValue;
	
    	RIO_VarField* entity_repelSpeed = findField("repelSpeed", entityRootFields);
    	if (entity_repelSpeed != NULL)
        	(*entity)->repelSpeed = entity_repelSpeed->floatValue;

		RIO_VarField* entity_isSolid = findField("isSolid", entityRootFields);
    	if (entity_isSolid != NULL)
        	(*entity)->isSolid = entity_isSolid->boolValue;

		RIO_VarField* entity_isImmovable = findField("isImmovable", entityRootFields);
    	if (entity_isImmovable != NULL)
        	(*entity)->isImmovable = entity_isImmovable->boolValue;

    	//---- Animation infos
    	RIO_VarField* entity_speedAnimation = findField("speedAnimation", entityRootFields);
    	if (entity_speedAnimation != NULL)
        	(*entity)->speedAnimation = entity_speedAnimation->floatValue;
	
		// For each state
    	int tileSetState;
    	for (tileSetState; tileSetState < NB_STATES; tileSetState++)
    	{
        	char strFieldState[256] = "";
        	// Search the corresponding tileset field
        	sprintf(strFieldState, "tileSet=%s", STATE_ENTITY_STRING[tileSetState]);
        	RIO_VarField* entity_tileSet = findField(strFieldState, entityRootFields);
        	// If a tileset has been found
        	if (entity_tileSet != NULL)
        	{
        		// get the tile size and the tile path
            	RIO_VarField* field_tileSize = findField("tileSize", entity_tileSet);
            	RIO_VarField* field_path = findField("path", entity_tileSet);
            	// Create the tileset
            	(*entity)->tileSet[tileSetState] = createTileSetEntity(renderer, field_tileSize->intValue, transparencyColor, field_path->stringValue);
				// Get the offset informations of each position
				RIO_VarField* field_offset = findField("offset", entity_tileSet);
				// If an offset is defined correctly
            	if (field_offset != NULL &&												// Exists
                	field_offset->hasSubFields && field_offset->subFields != NULL)		// has subfields
            	{
            		// Initialize the offset according to the number of direction of the tileSet
            		int nbDirection = (*entity)->tileSet[tileSetState]->nbDirection;
            		(*entity)->offsets[tileSetState][0] = malloc(sizeof(int)*nbDirection);
            		(*entity)->offsets[tileSetState][1] = malloc(sizeof(int)*nbDirection);
            		int i;
            		for(i = 0; i < nbDirection; i++)
            		{
                		(*entity)->offsets[tileSetState][0][i] = 0;
                		(*entity)->offsets[tileSetState][1][i] = 0;
            		}
					
					// For each subfields of the offset (each directions)
                	for(i = 0; i < field_offset->nbSubFields; i++)
                	{
                		// Get the direction given by the field
                		RIO_DirectionEntity directionEntity = directionEntity_StringToEnum(field_offset->subFields[i]->name);
                		// If it is a valid direction
                		if (0 <= directionEntity && directionEntity < nbDirection)
                		{
                			// Set the x and y offset for this direction and this state
                    		RIO_VarField* xOffset = findField("x", field_offset->subFields[i]);
                    		if (xOffset != NULL)
                        		(*entity)->offsets[tileSetState][0][directionEntity] = xOffset->intValue;
                    		RIO_VarField* yOffset = findField("y", field_offset->subFields[i]);
                    		if (yOffset != NULL)
                        		(*entity)->offsets[tileSetState][1][directionEntity] = yOffset->intValue;
                        }
                	}
            	}
	
            	// initialize the attackCollisionBox according to the number of direction of the attack tileSet
            	if (tileSetState == ATTACKING && (*entity)->tileSet[ATTACKING] != NULL)
            	{
            		int nbDirection = (*entity)->tileSet[ATTACKING]->nbDirection;
                	(*entity)->attackCollisionBox = malloc(sizeof(SDL_Rect)*nbDirection);
                	int i;
                	for(i = 0; i < nbDirection; i++)
                	{
                    	(*entity)->attackCollisionBox[i] = (SDL_Rect) {0, 0, 0, 0};
                	}
            	}
        	}
    	}
    	
        //---- Collision box
    	if ((*entity)->tileSet[IDLE] != NULL)
    	{
    		int idleTileSize = (*entity)->tileSet[IDLE]->tileSize;
        	RIO_VarField* entity_collisionBox = findField("collisionBox", entityRootFields);
    		if (entity_collisionBox != NULL)
        	{
        		int collisionBoxLeft = 0;
        		RIO_VarField* entity_offset = findField("leftOffset", entity_collisionBox);
        		if (entity_offset != NULL)
        			collisionBoxLeft = entity_offset->intValue;
        			
        		int collisionBoxTop = 0;
        		entity_offset = findField("topOffset", entity_collisionBox);
        		if (entity_offset != NULL)
        			collisionBoxTop = entity_offset->intValue;
        			
        		int collisionBoxRight = 0;
        		entity_offset = findField("rightOffset", entity_collisionBox);
        		if (entity_offset != NULL)
        			collisionBoxRight = entity_offset->intValue;
        			
        		int collisionBoxBottom = 0;
        		entity_offset = findField("bottomOffset", entity_collisionBox);
        		if (entity_offset != NULL)
        			collisionBoxBottom = entity_offset->intValue;
        			
        		float width = idleTileSize + collisionBoxRight - collisionBoxLeft;
				float height = idleTileSize + collisionBoxBottom - collisionBoxTop;
				updateCollisionBox(*entity, collisionBoxLeft, collisionBoxTop, width, height);
			}
		}
		
        //---- Attack collision box
        if ((*entity)->tileSet[ATTACKING] != NULL)
    	{
        	int nbDirection = (*entity)->tileSet[ATTACKING]->nbDirection;
        	RIO_VarField* entity_attackCollisionBox = findField("attackCollisionBox", entityRootFields);
        	// If attack collision boxes are correctly defined
    		if (entity_attackCollisionBox != NULL &&														// Exists
            	entity_attackCollisionBox->hasSubFields && entity_attackCollisionBox->subFields != NULL)	// has subfields
        	{
        		// For each subfields of the attack collision boxes (each directions)
        		int i;
        		for(i = 0; i < entity_attackCollisionBox->nbSubFields; i++)
        		{
            		// Get the direction given by the field
            		RIO_DirectionEntity directionEntity = directionEntity_StringToEnum(entity_attackCollisionBox->subFields[i]->name);
            		// If it is a valid direction
            		if (0 <= directionEntity && directionEntity < nbDirection)
            		{
                		// Set the x and y offset, the width and the heigth of the attack collision box for this direction
                		RIO_VarField* xOffset = findField("xOffset", entity_attackCollisionBox->subFields[i]);
                		if (xOffset != NULL)
                    		(*entity)->attackCollisionBox[directionEntity].x = xOffset->intValue;
                		RIO_VarField* yOffset = findField("yOffset", entity_attackCollisionBox->subFields[i]);
                		if (yOffset != NULL)
                    		(*entity)->attackCollisionBox[directionEntity].y = yOffset->intValue;
                    	RIO_VarField* width = findField("width", entity_attackCollisionBox->subFields[i]);
                		if (width != NULL)
                    		(*entity)->attackCollisionBox[directionEntity].w = width->intValue;
                		RIO_VarField* height = findField("height", entity_attackCollisionBox->subFields[i]);
                		if (height != NULL)
                    		(*entity)->attackCollisionBox[directionEntity].h = height->intValue;
            		}
        		}
        	}
        }
    }
    else
    	error = RIO_ERROR_NULLFILE;
    return error;
}

void showEntity(RIO_Entity* entity, SDL_Renderer* renderer, RIO_Camera* camera)
{
	// If the entity, the renderer and the camera exists
	if (entity != NULL && entity->tileSet[entity->state] != NULL && renderer != NULL && camera != NULL)
	{
		int x = entity->x;
		int y = entity->y;
		// We create a rectangle which will be use to render the texture
		if (entity->offsets[entity->state][0] != NULL &&
			entity->offsets[entity->state][1] != NULL)
		{
			x += entity->offsets[entity->state][0][entity->indexAnimation];
			y += entity->offsets[entity->state][1][entity->indexAnimation];
		}

		SDL_Rect tileRect = {x, y, entity->tileSet[entity->state]->tileSize, entity->tileSet[entity->state]->tileSize};
		// We create a rectangle which will be use to know if the texture we want to render is visible on the window
		SDL_Rect windowRect = {0, 0, camera->windowWidth, camera->windowHeight};
		// We update the rectangle with the camera
		updateTextureRectWithCamera(camera, &tileRect);

		// If the texture is visible on the window
		if (SDL_HasIntersection(&tileRect, &windowRect))
		{
			// Draw the texture of the entity
			SDL_RenderCopy(renderer, entity->tileSet[entity->state]->textureData[entity->indexAnimation][entity->frameAnimation], NULL, &tileRect);
		}
	}
}

void showEntities(RIO_ListEntity* listEntity, SDL_Renderer* renderer, RIO_Camera* camera)
{
	// If their is a list of entities
	if (listEntity != NULL)
	{
		// We create a list that will be used to store all the entities that we drew
		RIO_Entity* drawnEntities[listEntity->size];
		int indexEntity; // use to navigate through the list of entity
		int indexDrawnEntity; // use to navigate through the list of entity that has already been drawn
		// We set the list to NULL
		for(indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
		{
			drawnEntities[indexEntity] = NULL;
		}

		RIO_Entity* entityToDraw = NULL; // use to store the entity we will draw
		bool alreadyDrew = false; // use to know if the entity as already been drawn
		// We start a loop
		do
		{
			// We reset the entity to draw
			entityToDraw = NULL;
			// For each entities
			for(indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
			{
				// If the entity exists in the list of entities
				if (listEntity->entities[indexEntity] != NULL)
				{
					// We reset the flag use to know if it has been draw
					alreadyDrew = false;
					// For each drawn entities
					for(indexDrawnEntity = 0; indexDrawnEntity < listEntity->size; indexDrawnEntity++)
					{
						// If the entity we are checking is one of the already drawn entity
						if (listEntity->entities[indexEntity] == drawnEntities[indexDrawnEntity])
							// We set the flag to true to indicate that it has already been drawn
							alreadyDrew = true;
					}
					// If the entity has not already be drawn and if we still have no entities to draw or if his y coordinate is lower
					if (!alreadyDrew && (entityToDraw == NULL || listEntity->entities[indexEntity]->y < entityToDraw->y))
					{
						// We set a new entity to draw
						entityToDraw = listEntity->entities[indexEntity];
					}
				}
			}
			// If their is an entity to be drawn
			if (entityToDraw != NULL)
			{
				// We show the entity to draw in the renderer
				showEntity(entityToDraw, renderer, camera);

				bool stopLoop = false; // use to know if we need to stop the loot
				// We search for an empty space in the list of the already drawn entity
				indexDrawnEntity = 0;
				do
				{
					// once we find an empty space
					if (drawnEntities[indexDrawnEntity] == NULL)
					{
						// We fill it with the entity We just drew
						drawnEntities[indexDrawnEntity] = entityToDraw;
						// We say that we need to stop the loop
						stopLoop = true;
					}
					// We increase the index
					indexDrawnEntity++;
				// We continue if we're not out of range or if we still haven't found an empty space
				}while (indexDrawnEntity < listEntity->size && !stopLoop);
			}
		// We continue if the entity to draw was not NULL
		}while (entityToDraw != NULL);
	}
}

void moveMultiPlayerCameras(RIO_ListCamera* listCamera, float movementRatio, RIO_Entity* player1, RIO_Entity* player2, RIO_Entity* player3, RIO_Entity* player4)
{
	if (listCamera != NULL && listCamera->size > 0)
	{
		RIO_Entity* entityCamera[4];
		entityCamera[0] = player1;
		entityCamera[1] = player2;
		entityCamera[2] = player3;
		entityCamera[3] = player4;
		
		int indexPlayer;
		for (indexPlayer = 0; indexPlayer < listCamera->size; indexPlayer++)
		{
			if (indexPlayer < 4 && entityCamera[indexPlayer] != NULL)
			{
				float posCamera[2];
				getCenterPosEntity(entityCamera[indexPlayer], posCamera);
				moveCameraToPoint(listCamera->cameras[indexPlayer], posCamera[0], posCamera[1], movementRatio);
			}
		}
	}
}

void updatePositionEntities(RIO_ListEntity* listEntity, RIO_Map* map)
{
	int indexEntity;
	// Update the speed of all entities compare to non solid entities
	updateEntitiesCollisionWithEntities(listEntity);

	// Update the speed vector of all entities
	for(indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
	{
		RIO_Entity* entity = listEntity->entities[indexEntity];
		// We update the speed vector of the entity according to it's own deceleration and acceleration
		updateSpeedVectorFromAcceleration(entity);
		// We update the speed vector of the entity if it is pushed (only if it's solid and movable)
		updateSpeedVectorOfSolidEntity(listEntity, entity);
		// We update the speed vector of other entities if it pushes something (only if it's solid and immovable)
		updateImmovableEntityCollisionWithEntities(listEntity, entity);
	}
	for(indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
	{
		RIO_Entity* entity = listEntity->entities[indexEntity];
		if (entity != NULL && entity->isSolid)
		{
			// We crop speed vector of the entity according to the map and solid entities so it doesn't go out of bounds
			applySpeedVector(map, listEntity, entity);
		}
	}
	for(indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
	{
		RIO_Entity* entity = listEntity->entities[indexEntity];
		if (entity != NULL && !entity->isSolid)
		{
			// We crop speed vector of the entity according to the map and solid entities so it doesn't go out of bounds
			applySpeedVector(map, listEntity, entity);
		}
	}
}

void updateAnimationEntity(RIO_Entity* entity, int framePerSecond)
{
	// If the entity exists
	if (entity != NULL)
	{
		// If the entity needs to be animated (is moving)
		switch(entity->state)
		{
			case IDLE:
			case MOVING :
				upFrameAnimationWithSpeed(entity, framePerSecond);
				entity->animationClock++;
				break;
			case ATTACKING:
				switchAttackCollisionActivation(entity, 0.1, framePerSecond);
				upFrameAnimationAtTiming(entity, 0.2, framePerSecond);
				upFrameAnimationAtTiming(entity, 0.4, framePerSecond);
				switchAttackCollisionActivation(entity, 0.4, framePerSecond);
				unlockStateAtTiming(entity, 0.6, framePerSecond);
				entity->animationClock++;
				break;
			case KNOCKBACK :
				unlockStateAtTiming(entity, 0.5, framePerSecond);
				entity->animationClock++;
				break;
			case DEAD:
				upFrameAnimationUntilEnd(entity, framePerSecond);
				entity->animationClock++;
				break;
			default:
				// We reset the animation clock
				entity->animationClock = 0;
				// We reset the animation frame
				entity->frameAnimation = 0;
		}
	}
}

void updateAnimationEntities(RIO_ListEntity* listEntity, int framePerSecond)
{
	// For each entities
	int indexEntity;
	for(indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
	{
		// We update the animation of the entities
		updateAnimationEntity(listEntity->entities[indexEntity], framePerSecond);
	}
}

void updateStateEntityWithInputs(RIO_Entity* entity, RIO_KeyboardInput* keyboardInput, RIO_TypePlayer player)
{
	// Use to know the direction of the entity
	float xDir = 0; // On the x axis
	float yDir = 0; // On the y axis

	// If the left arrow is pressed
	if (keyboardInput->keyPressed[player][LEFT_KEY])
		// We update the direction on the x axis
		xDir--;
	// If the up arrow is pressed
	if (keyboardInput->keyPressed[player][UP_KEY])
		// We update the direction on the y axis
		yDir--;
	// If the right arrow is pressed
	if (keyboardInput->keyPressed[player][RIGHT_KEY])
		// We update the direction on the x axis
		xDir++;
	// If the down arrow is pressed
	if (keyboardInput->keyPressed[player][DOWN_KEY])
		// We update the direction on the y axis
		yDir++;

	// If the attack button has been pressed
	if (keyboardInput->keyPressed[player][ATTACK_KEY] || entity->stateIsLock)
	{
		changeStateEntity(entity, ATTACKING);
	}
	// If no direction has been pressed
	else if (fabsf(xDir) < MOVEMENT_THRESHOLD && fabsf(yDir) < MOVEMENT_THRESHOLD)
	{
		changeStateEntity(entity, IDLE);
	}
	else
	{
		// We normalise the direction and move towards it
		float angle = atan2(yDir, xDir);
		moveEntityToDirection(entity, angle);
	}
}

void updateEntityCollisionWithEntities(RIO_Entity* entity, RIO_ListEntity* listEntity)
{
	// If the entity and the list of entities exist
	if (entity != NULL && listEntity != NULL && entity->state != DEAD)
	{
		float speedVector[2] = {0, 0}; // use to calculate the sum of the speed vectors

		SDL_Rect tileRect = {0, 0, 0, 0}; // use to store the collision box of an entity
		int centerPoint[2] = {0, 0}; // use store the center of an entity

		// We create a rectangle which correspond to the collision box of the entity
		SDL_Rect entityTileRect = {
			entity->x + entity->collisionBox.x,
			entity->y + entity->collisionBox.y,
			entity->collisionBox.w,
			entity->collisionBox.h};
		// We calculate the center point of the entity we're updating
		int entityCenterPoint[2] = {
			entityTileRect.x + entityTileRect.w/2,
			entityTileRect.y + entityTileRect.h/2};
		// For each entity in the list
		int i;
		for(i = 0; i < listEntity->size; i++)
		{
			RIO_Entity* chekingEntity = listEntity->entities[i];
			// If the entity we're checking on the list is not our entity
			if (chekingEntity != entity && chekingEntity != NULL)
			{
				if (chekingEntity->state != DEAD)
				{
					///////////////
					// Collision //
					///////////////
					if (!entity->isImmovable)
					{
						// We create a rectangle which correspond to the collision of it
						tileRect.x = chekingEntity->x + chekingEntity->collisionBox.x;
						tileRect.y = chekingEntity->y + chekingEntity->collisionBox.y;
						tileRect.w = chekingEntity->collisionBox.w;
						tileRect.h = chekingEntity->collisionBox.h;
						SDL_Rect intersectionBox = {0, 0, 0, 0};
						// If it is colliding with our entity
						if (SDL_IntersectRect(&entityTileRect, &tileRect, &intersectionBox))
						{
							// We calculate his center point
							centerPoint[0] = tileRect.x + tileRect.w/2;
							centerPoint[1] = tileRect.y + tileRect.h/2;
							// We calculate the angle of the repeling vector
							float angle = atan2(entityCenterPoint[1] - centerPoint[1], entityCenterPoint[0] - centerPoint[0]);
							// We update the speedVector
							speedVector[0] += chekingEntity->repelSpeed * cos(angle);
							speedVector[1] += chekingEntity->repelSpeed * sin(angle);
						}
					}

					////////////
					// Attack //
					////////////

					// If it is attacking
					if (chekingEntity->attackCollisionIsActive && chekingEntity->attackCollisionBox != NULL &&
						entity->type != NEUTRAL &&
						(chekingEntity->friendlyFire || (chekingEntity->type != entity->type)))
					{
						// We create a rectangle which correspond to the attack collision of it
						tileRect.x = chekingEntity->x + chekingEntity->attackCollisionBox[chekingEntity->indexAnimation].x;
						tileRect.y = chekingEntity->y + chekingEntity->attackCollisionBox[chekingEntity->indexAnimation].y;
						tileRect.w = chekingEntity->attackCollisionBox[chekingEntity->indexAnimation].w;
						tileRect.h = chekingEntity->attackCollisionBox[chekingEntity->indexAnimation].h;
						// If it is attacked with our entity
						if (SDL_HasIntersection(&entityTileRect, &tileRect))
						{
							// We calculate the center point of the attacking entity
							centerPoint[0] = chekingEntity->x + chekingEntity->collisionBox.x + chekingEntity->collisionBox.w/2;
							centerPoint[1] = chekingEntity->y + chekingEntity->collisionBox.y + chekingEntity->collisionBox.h/2;
							// We calculate the angle of the repeling vector
							float angle = atan2(entityCenterPoint[1] - centerPoint[1], entityCenterPoint[0] - centerPoint[0]);
							// We update the speedVector
							speedVector[0] += chekingEntity->repelSpeed*4 * cos(angle);
							speedVector[1] += chekingEntity->repelSpeed*4 * sin(angle);

							// We update the damages and the state
							if (entity->state != KNOCKBACK)
								applyDamageToEntity(entity, chekingEntity->damage);
							changeStateEntity(entity, KNOCKBACK);
						}
					}
				}
			}
		}

		if (entity->state != DEAD)
		{
			// We calculte the current max speed the entity could have after we added the speed
			float maxSpeed = fabsf(sqrt(pow(speedVector[0], 2) + pow(speedVector[1], 2)) + entity->maxSpeed);
			// We update the speed of the entity
			entity->speedVector[0] += speedVector[0];
			entity->speedVector[1] += speedVector[1];

			// We calculte the current speed of the entity
			float currentSpeed = sqrt(pow(entity->speedVector[0], 2) + pow(entity->speedVector[1], 2));
			// If the speed is greater the the maximum speed allowed
			if (fabsf(currentSpeed) > maxSpeed)
			{
				// We reduce the speed
				entity->speedVector[0] *=  maxSpeed/fabsf(currentSpeed);
				entity->speedVector[1] *=  maxSpeed/fabsf(currentSpeed);
			}
			// We calculte the new current speed of the entity
			currentSpeed = sqrt(pow(entity->speedVector[0], 2) + pow(entity->speedVector[1], 2));
			// If the speed is greater the the maximum speed allowed
			if (fabsf(currentSpeed) > GLOBAL_MAX_SPEED)
			{
				// We reduce the speed
				entity->speedVector[0] *=  GLOBAL_MAX_SPEED/fabsf(currentSpeed);
				entity->speedVector[1] *=  GLOBAL_MAX_SPEED/fabsf(currentSpeed);
			}
		}
	}
}

void updateEntitiesCollisionWithEntities(RIO_ListEntity* listEntity)
{
	// For each entities
	int indexEntity;
	for(indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
	{
		// We update its speed according its collision with other entities
		updateEntityCollisionWithEntities(listEntity->entities[indexEntity], listEntity);
	}
}

bool checkBoxColideWithMap(RIO_Map* map, SDL_Rect* collisionBox, int* overlapVector)
{
	bool isColliding = false; // Will be update if the entity collide with a part of the map
	// If the map is valid
	if (map != NULL)
	{
		int xOverlap = 0; // use to know how far the collision box is into a wall on the x axis
		int yOverlap = 0; // use to know how far the collision box is into a wall on the y axis
		SDL_Rect intersectionBox = {0, 0, 0, 0}; // use to store the intersection box
		SDL_Rect tempCollisionBox = {0, 0, map->tileSet->tileSize, map->tileSet->tileSize};
		int tileIndex; // use to check the list of tile info
		// For each tile of the map
		int col, line;
		for (col = 0; col < map->width; col++)
		{
			for (line = 0; line < map->height; line++)
			{
				// For each tiles in the tiles info of the tileSet
				for(tileIndex = 0; tileIndex < map->tileSet->nbTileInfo; tileIndex++)
				{
					// If the tile info is not NULL
					if (map->tileSet->tilesInfo[tileIndex] != NULL)
					{
						// If the tile is in the tiles info and if it is solid
						if (map->mapData[col][line] == map->tileSet->tilesInfo[tileIndex]->id &&
							map->tileSet->tilesInfo[tileIndex]->isSolid)
						{
							tempCollisionBox.x = col * map->tileSet->tileSize;
							tempCollisionBox.y = line * map->tileSet->tileSize;
							// If we're on the collision box
							if (SDL_IntersectRect(&tempCollisionBox, collisionBox, &intersectionBox))
							{
								// We're colliding
								isColliding = true;
								// If the width of the intersection box is superior to the current xOverlap
								if (intersectionBox.w > xOverlap)
									// We update the xOverlap
									xOverlap = intersectionBox.w;
								// If the width of the intersection box is superior to the current yOverlap
								if (intersectionBox.h > yOverlap)
									// We update the yOverlap
									yOverlap = intersectionBox.h;
							}
						}
					}
				}
			}
		}
		// If the overlapVector exists
		if (overlapVector != NULL)
		{
			// We update the overlap vector
			overlapVector[0] = xOverlap;
			overlapVector[1] = yOverlap;
		}
	}
	// Then, we return the colliding value
	return isColliding;
}

bool checkBoxColideWithEntities(RIO_ListEntity* listEntity, RIO_Entity* checkedEntity, SDL_Rect* collisionBox, int* overlapVector, bool isSolid)
{
	bool isColliding = false; // Will be update if the entity collide with a part of the map
	// If the map is valid
	if (listEntity != NULL)
	{
		int xOverlap = 0; // use to know how far the collision box is into a wall on the x axis
		int yOverlap = 0; // use to know how far the collision box is into a wall on the y axis
		SDL_Rect intersectionBox = {0, 0, 0, 0}; // use to store the intersection box
		SDL_Rect tempCollisionBox = {0, 0, 16, 16};
		int tileIndex; // use to check the list of tile info
		// For each entities
		int indexEntity;
		for (indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
		{
			RIO_Entity* tempEntity = listEntity->entities[indexEntity];
			// If the entity is not NULL
			if (tempEntity != NULL && 
				(checkedEntity == NULL || tempEntity != checkedEntity) &&
				tempEntity->state != DEAD)
			{
				// If the entity correspond to the isSolid boolean
				if (tempEntity->isSolid == isSolid)
				{
					tempCollisionBox.x = tempEntity->x + tempEntity->collisionBox.x;
					tempCollisionBox.y = tempEntity->y + tempEntity->collisionBox.y;
					tempCollisionBox.w = tempEntity->collisionBox.w;
					tempCollisionBox.h = tempEntity->collisionBox.h;
					// If we're on the collision box
					if (SDL_IntersectRect(&tempCollisionBox, collisionBox, &intersectionBox))
					{
						// We're colliding
						isColliding = true;
						// If the width of the intersection box is superior to the current xOverlap
						if (intersectionBox.w > xOverlap)
							// We update the xOverlap
							xOverlap = intersectionBox.w;
						// If the width of the intersection box is superior to the current yOverlap
						if (intersectionBox.h > yOverlap)
							// We update the yOverlap
							yOverlap = intersectionBox.h;
					}
				}
			}
		}
		// If the overlapVector exists
		if (overlapVector != NULL)
		{
			// We update the overlap vector
			overlapVector[0] = xOverlap;
			overlapVector[1] = yOverlap;
		}
	}
	// Then, we return the colliding value
	return isColliding;
}

void applySpeedVector(RIO_Map* map, RIO_ListEntity* listEntity, RIO_Entity* entity)
{
	// If the entity exists
	if (entity != NULL)
	{
		// We create a 2D array
		float speedVector[2] = {0, 0};
		// We set his value to the x and y speed of the entity based on his current speed and his direction
		speedVector[0] = entity->speedVector[0];
		speedVector[1] = entity->speedVector[1];
		// If the map exists
		if (map != NULL && listEntity != NULL)
		{
			int overlapMapVector[2] = {0, 0}; // use to store the intersection between the entity and the map
			int overlapSolidEntitiesVector[2] = {0, 0}; // use to store the intersection between the entity and the solids entities
			int overlapEntitiesVector[2] = {0, 0}; // use to store the intersection between the solid entity and the entities
			// We create a dummy collision box in the futur x position using the speed on the x axis
			SDL_Rect tempCollisionBox = {
				entity->collisionBox.x + speedVector[0],
				entity->collisionBox.y + speedVector[1],
				entity->collisionBox.w,
				entity->collisionBox.h};
				
			tempCollisionBox.x = entity->x + entity->collisionBox.x + speedVector[0];
			tempCollisionBox.y = entity->y + entity->collisionBox.y;
			checkBoxColideWithMap(map, &tempCollisionBox, overlapMapVector);
			checkBoxColideWithEntities(listEntity, entity, &tempCollisionBox, overlapSolidEntitiesVector, true);
			if (entity->isSolid && !entity->isImmovable)
				checkBoxColideWithEntities(listEntity, entity, &tempCollisionBox, overlapEntitiesVector, false);
			int maxXOverlap = fmax(fmax(overlapMapVector[0], overlapSolidEntitiesVector[0]), overlapEntitiesVector[0]);

			tempCollisionBox.x = entity->x + entity->collisionBox.x;
			tempCollisionBox.y = entity->y + entity->collisionBox.y + speedVector[1];
			checkBoxColideWithMap(map, &tempCollisionBox, overlapMapVector);
			checkBoxColideWithEntities(listEntity, entity, &tempCollisionBox, overlapSolidEntitiesVector, true);
			if (entity->isSolid && !entity->isImmovable)
				checkBoxColideWithEntities(listEntity, entity, &tempCollisionBox, overlapEntitiesVector, false);
			int maxYOverlap = fmax(fmax(overlapMapVector[1], overlapSolidEntitiesVector[1]), overlapEntitiesVector[1]);
			
			if (maxXOverlap < maxYOverlap)
			{
				speedVector[0] = sign(speedVector[0]) * (fabsf(speedVector[0]) - maxXOverlap);

				tempCollisionBox.x = entity->x + entity->collisionBox.x + speedVector[0];
				tempCollisionBox.y = entity->y + entity->collisionBox.y + speedVector[1];
				checkBoxColideWithMap(map, &tempCollisionBox, overlapMapVector);
				checkBoxColideWithEntities(listEntity, entity, &tempCollisionBox, overlapSolidEntitiesVector, true);
				if (entity->isSolid && !entity->isImmovable)
					checkBoxColideWithEntities(listEntity, entity, &tempCollisionBox, overlapEntitiesVector, false);
				maxYOverlap = fmax(fmax(overlapMapVector[1], overlapSolidEntitiesVector[1]), overlapEntitiesVector[1]);
				speedVector[1] = sign(speedVector[1]) * (fabsf(speedVector[1]) - maxYOverlap);
			}
			else
			{
				speedVector[1] = sign(speedVector[1]) * (fabsf(speedVector[1]) - maxYOverlap);
				
				tempCollisionBox.x = entity->x + entity->collisionBox.x + speedVector[0];
				tempCollisionBox.y = entity->y + entity->collisionBox.y + speedVector[1];
				checkBoxColideWithMap(map, &tempCollisionBox, overlapMapVector);
				checkBoxColideWithEntities(listEntity, entity, &tempCollisionBox, overlapSolidEntitiesVector, true);
				if (entity->isSolid && !entity->isImmovable)
					checkBoxColideWithEntities(listEntity, entity, &tempCollisionBox, overlapEntitiesVector, false);
				maxXOverlap = fmax(fmax(overlapMapVector[0], overlapSolidEntitiesVector[0]), overlapEntitiesVector[0]);
				speedVector[0] = sign(speedVector[0]) * (fabsf(speedVector[0]) - maxXOverlap);
			}
		}
		// We update the coordinate and the speed of the entity with the calculated speed
		entity->x += speedVector[0];
		entity->y += speedVector[1];
		if (speedVector[0] != entity->speedVector[0])
			entity->speedVector[0] = 0;
		if (speedVector[1] != entity->speedVector[1])
			entity->speedVector[1] = 0;
	}
}

void updateSpeedVectorOfSolidEntity(RIO_ListEntity* listEntity, RIO_Entity* entity)
{
	// If the entity exists, is solid and not immovable
	if (entity != NULL && entity->isSolid && !entity->isImmovable)
	{
		// If the map exists
		if (listEntity != NULL)
		{
			SDL_Rect intersectionBox = {0, 0, 0, 0}; // use to store the intersection box
			SDL_Rect collisionBox = {
				entity->x + entity->collisionBox.x, 
				entity->y + entity->collisionBox.y, 
				entity->collisionBox.w, 
				entity->collisionBox.h};
			// For each entities
			int indexEntity;
			for (indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
			{
				RIO_Entity* tempEntity = listEntity->entities[indexEntity];
				// If the other entity is not NULL and we're not checking the solid entity itself
				if (tempEntity != NULL && 
					tempEntity != entity &&
					tempEntity->state != DEAD)
				{
					// We get the collision box he will have after his movement
					SDL_Rect tempCollisionBox = {
						tempEntity->x + tempEntity->collisionBox.x + tempEntity->speedVector[0],
						tempEntity->y + tempEntity->collisionBox.y + tempEntity->speedVector[1],
						tempEntity->collisionBox.w,
						tempEntity->collisionBox.h};
					// If the solid entity is inside his collision box
					if (SDL_IntersectRect(&tempCollisionBox, &collisionBox, &intersectionBox))
					{
						if (fabsf(entity->y - tempEntity->y)/(collisionBox.h + tempCollisionBox.h) < 
							fabsf(entity->x - tempEntity->x)/(collisionBox.w + tempCollisionBox.w))
							entity->speedVector[0] = sign(tempEntity->speedVector[0]) * (intersectionBox.w);
						if (fabsf(entity->y - tempEntity->y)/(collisionBox.h + tempCollisionBox.h) > 
							fabsf(entity->x - tempEntity->x)/(collisionBox.w + tempCollisionBox.w))
							entity->speedVector[1] = sign(tempEntity->speedVector[1]) * (intersectionBox.h);
					}
				}
			}
		}
	}
}

void updateImmovableEntityCollisionWithEntities(RIO_ListEntity* listEntity, RIO_Entity* entity)
{
	// If the entity exists, is solid and immovable
	if (entity != NULL && entity->isSolid && entity->isImmovable)
	{
		// If the map exists
		if (listEntity != NULL)
		{
			SDL_Rect intersectionBox = {0, 0, 0, 0}; // use to store the intersection box
			SDL_Rect collisionBox = {
				entity->x + entity->collisionBox.x + entity->speedVector[0], 
				entity->y + entity->collisionBox.y + entity->speedVector[1], 
				entity->collisionBox.w, 
				entity->collisionBox.h};
			// For each entities
			int indexEntity;
			for (indexEntity = 0; indexEntity < listEntity->size; indexEntity++)
			{
				RIO_Entity* tempEntity = listEntity->entities[indexEntity];
				// If the other entity is not NULL and we're not checking the solid entity itself
				if (tempEntity != NULL && 
					tempEntity != entity &&
					tempEntity->state != DEAD &&
					!tempEntity->isImmovable)
				{
					// We get the collision box he will have after his movement
					SDL_Rect tempCollisionBox = {
						tempEntity->x + tempEntity->collisionBox.x + tempEntity->speedVector[0],
						tempEntity->y + tempEntity->collisionBox.y + tempEntity->speedVector[1],
						tempEntity->collisionBox.w,
						tempEntity->collisionBox.h};
					// If the solid entity is inside his collision box
					if (SDL_IntersectRect(&tempCollisionBox, &collisionBox, &intersectionBox))
					{
						if (fabsf(entity->y - tempEntity->y)/(collisionBox.h + tempCollisionBox.h) < 
							fabsf(entity->x - tempEntity->x)/(collisionBox.w + tempCollisionBox.w))
							tempEntity->speedVector[0] = sign(entity->speedVector[0]) * (intersectionBox.w);
						if (fabsf(entity->y - tempEntity->y)/(collisionBox.h + tempCollisionBox.h) > 
							fabsf(entity->x - tempEntity->x)/(collisionBox.w + tempCollisionBox.w))
							tempEntity->speedVector[1] = sign(entity->speedVector[1]) * (intersectionBox.h);
					}
				}
			}
		}
	}
}

void updateSpeedVectorFromAcceleration(RIO_Entity* entity)
{
	if (entity != NULL)
	{
		// If we're not moving on the x axis
		if (fabsf(entity->direction[0]) < MOVEMENT_THRESHOLD)
		{
			// If the entity would slow down more than the actual x speed
			if (entity->maxSpeed * entity->accelerationFactor > fabsf(entity->speedVector[0]))
				// We set the speed to 0
				entity->speedVector[0] = 0;
			else
				// We slow the entity down
				entity->speedVector[0] -= sign(entity->speedVector[0]) * entity->maxSpeed * entity->accelerationFactor;
		}
		else
		{
			// We update the speed
			entity->speedVector[0] += entity->direction[0] * entity->maxSpeed * entity->accelerationFactor;
			// If we go above the maximum speed
			if (fabsf(entity->speedVector[0]) > fabsf(entity->maxSpeed * entity->direction[0]))
				// We cap the speed at the maximum speed
				entity->speedVector[0] = entity->maxSpeed * entity->direction[0];
		}

		// If we're not moving on the y axis
		if (fabsf(entity->direction[1]) < MOVEMENT_THRESHOLD)
		{
			// If the entity would slow down more than the actual y speed
			if (entity->maxSpeed * entity->accelerationFactor > fabsf(entity->speedVector[1]))
				// We set the speed to 0
				entity->speedVector[1] = 0;
			else
				// We slow the entity down
				entity->speedVector[1] -= sign(entity->speedVector[1]) * entity->maxSpeed * entity->accelerationFactor;
		}
		else
		{
			// We update the speed
			entity->speedVector[1] += entity->direction[1] * entity->maxSpeed * entity->accelerationFactor;
			// If we go above the maximum speed
			if (fabsf(entity->speedVector[1]) > fabsf(entity->maxSpeed * entity->direction[1]))
				// We cap the speed at the maximum speed
				entity->speedVector[1] = entity->maxSpeed * entity->direction[1];
		}
	}
}

void applyDamageToEntity(RIO_Entity* entity, int damage)
{
	if (entity != NULL)
	{
		entity->currentHealth -= damage;
		if (entity->currentHealth < 0)
			entity->currentHealth = 0;
		if (entity->currentHealth <= 0)
			changeStateEntity(entity, DEAD);
	}
}

bool changeStateEntity(RIO_Entity* entity, RIO_StateEntity newState)
{
	if (entity != NULL && entity->state != DEAD)
	{
		// If the state is the same
		if (entity->state == newState)
			return true;

		// We get the priority of each states
		int statePriority[NB_STATES] = STATE_PRIORITIES;
		// If the priority of the new state isn't enought to interrupt the lock of the state
		if (statePriority[newState] < statePriority[entity->state] && entity->stateIsLock)
			return false;

		// If all the previous checks are ok, we change the state
		entity->animationClock = 0;
		entity->frameAnimation = 0;
		entity->attackCollisionIsActive = false;
		entity->state = newState;

		switch (newState)
		{
			case IDLE:
				entity->direction[0] = 0;
				entity->direction[1] = 0;
				break;
			case MOVING:
				break;
			case ATTACKING:
				entity->direction[0] = 0;
				entity->direction[1] = 0;
				entity->stateIsLock = true;
				break;
			case KNOCKBACK:
				entity->direction[0] = 0;
				entity->direction[1] = 0;
				entity->stateIsLock = true;
				break;
			case DEAD:
				entity->direction[0] = 0;
				entity->direction[1] = 0;
				entity->speedVector[0] = 0;
				entity->speedVector[1] = 0;
				entity->stateIsLock = true;
				break;
            default:
				entity->direction[0] = 0;
				entity->direction[1] = 0;
				break;
		}
		return true;
	}
	return false;
}

void getCenterPosEntity(RIO_Entity* entity, float* position)
{
	if (entity != NULL && position != NULL)
	{
		position[0] = entity->x + entity->collisionBox.x + entity->collisionBox.w/2;
		position[1] = entity->y + entity->collisionBox.y + entity->collisionBox.h/2;
	}
}

void getCenterPosEntities(RIO_Entity** entities, int nbEntity, float* position)
{
	if (entities != NULL)
	{
		position[0] = 0;
		position[1] = 0;
		int indexEntity;
		for (indexEntity = 0; indexEntity < nbEntity; indexEntity++)
		{
			if (entities[indexEntity] != NULL)
			{
				float tempPos[2];
				getCenterPosEntity(entities[indexEntity], tempPos);
				position[0] += tempPos[0];
				position[1] += tempPos[1];
			}
		}
		position[0] /= nbEntity;
		position[1] /= nbEntity;
	}
}

RIO_StateEntity aToStateEntity(char* str)
{
	if (strcmp(str, "MOVING") == 0)
		return MOVING;
	else if (strcmp(str, "ATTACKING") == 0)
		return ATTACKING;
	else if (strcmp(str, "KNOCKBACK") == 0)
		return KNOCKBACK;
	else if (strcmp(str, "DEAD") == 0)
		return DEAD;
	else
		return IDLE;
}

RIO_TypeEntity aToTypeEntity(char* str)
{
	if (strcmp(str, "NOTYPE") == 0)
		return NOTYPE;
	else if (strcmp(str, "PLAYER") == 0)
		return PLAYER;
	else if (strcmp(str, "ENEMY") == 0)
		return ENEMY;
	else
		return NOTYPE;
}

RIO_TypePath aToTypePath(char* str)
{
	if (strcmp(str, "ONE_WAY") == 0)
		return ONE_WAY;
	else if (strcmp(str, "BACK_AND_FORTH") == 0)
		return BACK_AND_FORTH;
	else if (strcmp(str, "LOOP") == 0)
		return LOOP;
	else
		return ONE_WAY;
}

void getCompleteEntityPathToCoord(RIO_Entity* entity, float x, float y, RIO_Map* map)
{
	if (entity != NULL && map != NULL)
	{
		int xStart = (entity->x + (entity->collisionBox.x + entity->collisionBox.w)/2)/map->tileSet->tileSize;
		int yStart = (entity->y + (entity->collisionBox.y + entity->collisionBox.h)/2)/map->tileSet->tileSize;
		node* startNode = initNode(xStart, yStart, 0, 0);
		node* endNode = initNode((int) x/map->tileSet->tileSize, (int) y/map->tileSet->tileSize, 0, 0);
		node* openSet = cpyNode(startNode);
		node* closedSet = NULL;
		node* path = NULL;
		while (path == NULL)
		{
			path = AStar(&openSet, &closedSet, startNode, endNode, map);
		}
		updatePathToCoordinate(&path, map->tileSet->tileSize);

		if (entity->nextNodeInPath != NULL)
			entity->nextNodeInPath = getClosetNodeToCoord(&path, entity->nextNodeInPath->x, entity->nextNodeInPath->y);
		else
			entity->nextNodeInPath = path;
		destructNodes(&entity->pathToFollow);
		entity->pathToFollow = path;

		destructNodes(&openSet);
		destructNodes(&closedSet);
		destructNodes(&startNode);
		destructNodes(&endNode);
	}
}

void getCompleteEntityPathToEntity(RIO_Entity* entity, RIO_Entity* entityToGo, RIO_Map* map)
{
	if (entityToGo != NULL && entity != NULL)
	{
		float x = entityToGo->x + (entityToGo->collisionBox.x + entityToGo->collisionBox.w)/2;
		float y = entityToGo->y + (entityToGo->collisionBox.y + entityToGo->collisionBox.h)/2;
		getCompleteEntityPathToCoord(entity, x, y, map);
		node* lastNode = getLastNode(&entity->pathToFollow);
		if (lastNode != NULL)
			lastNode->linkedNode = initNode(entityToGo->x, entityToGo->y, 0, 0);
	}
}

void moveEntityToDirection(RIO_Entity* entity, float angle)
{
	if (entity != NULL && changeStateEntity(entity, MOVING))
	{
		// We calculate the direction
		entity->direction[0] = cos(angle);
		entity->direction[1] = sin(angle);
		// Depending on the angle we update the sprite to use
		// Right sprite
		if (-M_PI/4 < angle && angle <= M_PI/4)
		{
			entity->indexAnimation = 1;
		}
		// Up sprite
		else if (-3*M_PI/4 < angle && angle <= -M_PI/4)
		{
			entity->indexAnimation = 0;
		}
		// Down sprite
		else if (M_PI/4 < angle && angle <= 3*M_PI/4)
		{
			entity->indexAnimation = 2;
		}
		// Right sprite
		else
		{
			entity->indexAnimation = 3;
		}
	}
}


bool moveEntityToCoord(RIO_Entity* entity, float x, float y, float dist)
{
	if (entity != NULL)
	{
		float actualDistance = sqrt(pow(entity->x - x, 2) + pow(entity->y - y, 2));
		if (actualDistance <= dist)
		{
			changeStateEntity(entity, IDLE);
			return true;
		}
		else
		{
			changeStateEntity(entity, MOVING);
		}

		// We calculate the direction
		float angle = atan2(y - entity->y, x - entity->x);
		moveEntityToDirection(entity, angle);
	}
	return false;
}

bool moveEntityAwayFromCoord(RIO_Entity* entity, float x, float y, float dist)
{
	if (entity != NULL)
	{
		float actualDistance = sqrt(pow(entity->x - x, 2) + pow(entity->y - y, 2));
		if (actualDistance >= dist && dist != -1)
		{
			changeStateEntity(entity, IDLE);
			return true;
		}
		else
		{
			changeStateEntity(entity, MOVING);
		}

		// We calculate the direction
		float angle = atan2(entity->y - y, entity->x - x);
		moveEntityToDirection(entity, angle);
	}
	return false;
}

bool followStaticPath(RIO_Entity* entity, float dist)
{
	if (entity != NULL)
	{
		if (entity->pathToFollow != NULL)
		{
			if (entity->nextNodeInPath == NULL)
			{
				node* temp = NULL;
				switch(entity->pathType)
				{
					case ONE_WAY:
						break;
					case BACK_AND_FORTH:
						// We reverse the path
						temp = getReversePath(&entity->pathToFollow);
						destructNodes(&entity->pathToFollow);
						entity->pathToFollow = temp;
						// We update the movement
						entity->nextNodeInPath = entity->pathToFollow;
						moveEntityToCoord(entity, entity->nextNodeInPath->x, entity->nextNodeInPath->y, dist);
						break;
					case LOOP:
						// We update the movement
						entity->nextNodeInPath = entity->pathToFollow;
						moveEntityToCoord(entity, entity->nextNodeInPath->x, entity->nextNodeInPath->y, dist);
						break;
				}
				return true;
			}

			if (moveEntityToCoord(entity, entity->nextNodeInPath->x, entity->nextNodeInPath->y, dist))
			{
				entity->nextNodeInPath = getNodeAfter(&entity->pathToFollow, entity->nextNodeInPath);
			}
		}
	}
	return false;
}

bool followDynamicPath(RIO_Entity* entity, float dist)
{
	if (entity != NULL)
	{
		if (entity->pathToFollow != NULL)
		{
			float currentDist = 0;
			node* lastNode = getLastNode(&entity->pathToFollow);
			if (lastNode != NULL)
			{
				currentDist = sqrt(pow(lastNode->x - entity->x, 2) + pow(lastNode->y - entity->y, 2));
			}
			while (currentDist > dist &&
				entity->nextNodeInPath != NULL &&
				moveEntityToCoord(entity, entity->nextNodeInPath->x, entity->nextNodeInPath->y, entity->maxSpeed))
			{
				free(popNode(&entity->pathToFollow));
				entity->nextNodeInPath = entity->pathToFollow;
			}

			// If we arrive at destination
			if (entity->nextNodeInPath == NULL || currentDist < dist)
			{
				changeStateEntity(entity, IDLE);
				return true;
			}
			else
			{
				changeStateEntity(entity, MOVING);
			}
		}
	}
	return false;
}

void upFrameAnimationWithSpeed(RIO_Entity* entity, int framePerSecond)
{
	if (entity != NULL)
	{
		// If enought frame has passed
		if (entity->animationClock%((int) floor(entity->speedAnimation * framePerSecond)) == 0)
		{
			// We update the animation
			entity->frameAnimation++;
			// If the animation frame is too high it get back to 0
			if (entity->tileSet[entity->state] != NULL)
				entity->frameAnimation %= entity->tileSet[entity->state]->nbAnimation;
		}
	}
}

bool upFrameAnimationUntilEnd(RIO_Entity* entity, int framePerSecond)
{
	if (entity != NULL)
	{
		if (entity->frameAnimation < entity->tileSet[entity->state]->nbAnimation - 1)
		{
			// If enought frame has passed
			if (entity->animationClock%((int) floor(entity->speedAnimation * framePerSecond)) == 0)
			{
				// We update the animation
				entity->frameAnimation++;
			}
			return false;
		}
	}
    return true;
}

void upFrameAnimationAtTiming(RIO_Entity* entity, float timing, int framePerSecond)
{
	if (entity != NULL)
	{
		// If enought frame has passed
		if (entity->animationClock == floor(timing * framePerSecond))
		{
			entity->frameAnimation++;
			// If the animation frame is too high it get back to 0
			if (entity->tileSet[entity->state] != NULL)
				entity->frameAnimation %= entity->tileSet[entity->state]->nbAnimation;
		}
	}
}

void unlockStateAtTiming(RIO_Entity* entity, float timing, int framePerSecond)
{
	if (entity != NULL)
	{
		if (entity->animationClock == floor(timing * framePerSecond))
		{
			// We stop the attack
			entity->stateIsLock = false;
		}
	}
}

void switchAttackCollisionActivation(RIO_Entity* entity, float timing, int framePerSecond)
{
	if (entity != NULL)
	{
		if (entity->animationClock == floor(timing * framePerSecond))
		{
			// We switch the flag on or off
			if (entity->attackCollisionIsActive)
				entity->attackCollisionIsActive = false;
			else
				entity->attackCollisionIsActive = true;
		}
	}
}

void showCollisionBoxEntity(RIO_Entity* entity, SDL_Renderer* renderer, RIO_Camera* camera)
{
	// If the entity, the renderer and the camera exists
	if (entity != NULL && renderer != NULL && camera != NULL)
	{
		int x = entity->x + entity->collisionBox.x;
		int y = entity->y + entity->collisionBox.y;

		SDL_Rect tileRect = {x, y, entity->collisionBox.w, entity->collisionBox.h};
		// We create a rectangle which will be use to know if the texture we want to render is visible on the window
		SDL_Rect windowRect = {0, 0, camera->windowWidth, camera->windowHeight};
		// We update the rectangle with the camera
		updateTextureRectWithCamera(camera, &tileRect);

		// If the square is visible on the window
		if (SDL_HasIntersection(&tileRect, &windowRect))
		{
			SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255);
			SDL_RenderDrawRect(renderer, &tileRect);
		}
	}
}

void showCollisionBoxEntities(RIO_ListEntity* listEntity, SDL_Renderer* renderer, RIO_Camera* camera)
{
	if (listEntity != NULL)
	{
		int i;
		for (i = 0; i < listEntity->size; i++)
			showCollisionBoxEntity(listEntity->entities[i], renderer, camera);
	}
}

void showAttackCollisionBoxEntity(RIO_Entity* entity, SDL_Renderer* renderer, RIO_Camera* camera)
{
	// If the entity, the renderer and the camera exists
	if (entity != NULL && renderer != NULL && camera != NULL && entity->attackCollisionIsActive)
	{
		SDL_Rect collisionBox = entity->attackCollisionBox[entity->indexAnimation];
		int x = entity->x + collisionBox.x;
		int y = entity->y + collisionBox.y;

		SDL_Rect tileRect = {x, y, collisionBox.w, collisionBox.h};
		// We create a rectangle which will be use to know if the texture we want to render is visible on the window
		SDL_Rect windowRect = {0, 0, camera->windowWidth, camera->windowHeight};
		// We update the rectangle with the camera
		updateTextureRectWithCamera(camera, &tileRect);

		// If the square is visible on the window
		if (SDL_HasIntersection(&tileRect, &windowRect))
		{
			SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
			SDL_RenderDrawRect(renderer, &tileRect);
		}
	}
}

void showHealthEntity(RIO_Entity* entity, SDL_Renderer* renderer, RIO_Camera* camera)
{
	if (entity != NULL && renderer != NULL && camera != NULL)
	{
		int x = entity->x;
		int y = entity->y - 4;
		int w = 16;
		if (entity->tileSet[IDLE] != NULL)
			w = entity->tileSet[IDLE]->tileSize;
		int h = 2;
		SDL_Rect barRect = {x, y, w, h};
		w = nmap(entity->currentHealth, 0, entity->maxHealth, 0, w);
		SDL_Rect healthRect = {x, y, w, h};

		// We create a rectangle which will be use to know if the texture we want to render is visible on the window
		SDL_Rect windowRect = {0, 0, camera->windowWidth, camera->windowHeight};
		// We update the rectangles with the camera
		updateTextureRectWithCamera(camera, &barRect);
		updateTextureRectWithCamera(camera, &healthRect);
		// If the square is visible on the window
		if (SDL_HasIntersection(&barRect, &windowRect))
		{
			SDL_SetRenderDrawColor(renderer, 220, 50, 50, 255);
			SDL_RenderFillRect(renderer, &barRect);
			SDL_SetRenderDrawColor(renderer, 50, 220, 50, 255);
			SDL_RenderFillRect(renderer, &healthRect);
		}
	}
}

void showHealthEntities(RIO_ListEntity* listEntity, RIO_TypeEntity type, SDL_Renderer* renderer, RIO_Camera* camera)
{
	if (listEntity != NULL)
	{
		int i;
		for (i = 0; i < listEntity->size; i++)
		{
			if (listEntity->entities[i] != NULL &&
				(type == NOTYPE || listEntity->entities[i]->type == type) &&
				listEntity->entities[i]->state != DEAD)
				showHealthEntity(listEntity->entities[i], renderer, camera);
		}
	}
}

RIO_TypeEntity typeEntity_StringToEnum(const char* typeStr)
{
    if (typeStr != NULL)
    {
        int i;
        for (i = 0; i < NB_TYPES; i++)
        {
            if (strcmp(typeStr, TYPE_ENTITY_STRING[i]) == 0)
                return i;
        }
    }
    return NOTYPE;
}

RIO_StateEntity stateEntity_StringToEnum(const char* stateStr)
{
    if (stateStr != NULL)
    {
        int i;
        for (i = 0; i < NB_STATES; i++)
        {
            if (strcmp(stateStr, STATE_ENTITY_STRING[i]) == 0)
                return i;
        }
    }
    return -1;
}

RIO_DirectionEntity directionEntity_StringToEnum(const char* directionStr)
{
    if (directionStr != NULL)
    {
        int i;
        for (i = 0; i < NB_DIRECTIONS; i++)
        {
            if (strcmp(directionStr, DIRECTION_ENTITY_STRING[i]) == 0)
                return i;
        }
    }
    return -1;
}
