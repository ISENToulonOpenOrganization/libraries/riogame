#ifndef RIO_FUNCTION_H
	#define RIO_FUNCTION_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#include <dirent.h>

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

#ifndef DT_REG
	#define DT_REG 8
#endif
#ifndef DT_DIR
	#define DT_DIR 4
#endif

#define RED_COLOR_TER 			"\033[0;31m"
#define BOLDRED_COLOR_TER 		"\033[1;31m"
#define GREEN_COLOR_TER 		"\033[0;32m"
#define BOLDGREEN_COLOR_TER 	"\033[1;32m"
#define YELLOW_COLOR_TER 		"\033[0;33m"
#define BOLDYELLOW_COLOR_TER 	"\033[1;33m"
#define BLUE_COLOR_TER 			"\033[0;34m"
#define BOLDBLUE_COLOR_TER 		"\033[1;34m"
#define MAGANTA_COLOR_TER 		"\033[0;35m"
#define BOLDMAGANTA_COLOR_TER 	"\033[1;35m"
#define CYAN_COLOR_TER 			"\033[0;36m"
#define BOLDCYAN_COLOR_TER 		"\033[1;36m"
#define RESET_COLOR_TER 		"\033[0m"

typedef struct
{
	float x;
	float y;
} RIO_Point;

typedef struct
{
    float x;
    float y;
    float angle;
} RIO_Line;

typedef struct
{
	RIO_Point* listPoint;
	int nbPoint;
} RIO_Shape;

/* @function
 *		initialize a shape
 *
 * @param
 *		nbPoint : 	number of point the shape can hape
 *
 * @return
 *		return a shape
 */
RIO_Shape* initShape(int nbPoint);

/* @function
 *		fre a shape from the memory
 *
 * @param
 *		shape : 	the shape to free
 */
void destructShape(RIO_Shape** shape);

/* @function
 *		transform a value which vary between vmin and vmax, to an other one that vary between min and max
 *
 * @param
 *		value : 	the reference value that we want to transform
 *		vmin :		the minimum the value can get
 *		vmax :		the maximum the value can get
 *		min :		the new minimum we want to set the value to
 *		max :		the new maximum we want to set the value to
 *
 * @return
 *		return a float number between min and max
 */
float nmap(float value, float vmin, float vmax, float min, float max);


/* @function
 *		use to get the sign of a value
 *
 * @param
 *		value : the value we want the sign from
 *
 * @return
 *		return -1 if negative, 1 if positive and 0 if equal to 0
 */
int sign(float value);

/* @function
 *		copy an existing list of float
 *
 * @param
 *		list : 	the original list we want to copy
 *		size :	the size of the list
 *
 * @return
 *		return a list of float
 */

float* cpyList(float* list, int size);

/* @function
 *		free a 2D matrice from the memory
 *
 * @param
 *		matrice : 	the matrice we want to free from the memory
 *		size :	the size of the first dimmension of the list
 *
 * @return
 *		return a list of float
 */
void destructMatrice2D(void** matrice, int size);

/* @function
 *		shuffle the values in a given list
 *
 * @param
 *		list : 	the list we want to shuffle
 *		size :	the size of the list
 */
void shuffleList(float* list, int size);

/* @function
 *		find the point where two lines intersect
 *
 * @param
 *		l1 : the first line
 *		l2 : the second line
 *
 * @return
 *      the crossing point of the two lines. If the two lines are parallel, return NULL
 */
RIO_Point* getCrossingPoint(RIO_Line* l1, RIO_Line* l2);

#endif
