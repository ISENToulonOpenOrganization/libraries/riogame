#ifndef RIO_FILE_H
	#define RIO_FILE_H

#include "rioFunction.h"
#include "rioType.h"

// error ID used when loading and saving
typedef enum RIO_ReadingState {RIO_READ_NAME = 0, RIO_READ_VALUE, RIO_READ_STOP} RIO_ReadingState;

typedef enum RIO_VarType {RIO_UNKNOWN_TYPE = -1, RIO_STRING_TYPE = 0, RIO_INT_TYPE, RIO_FLOAT_TYPE, RIO_BOOL_TYPE, RIO_FIELD_TYPE} RIO_VarType;

// Use to store informations extracted from a file. This structure can contain subfields
typedef struct RIO_VarField
{
	char name[256];                     // the name of the field
	RIO_VarType type;                   // the type of the field

	char stringValue[256];              // the string value of the field. It is suppose to always contain something whatever the type is
	float floatValue;                   // the floating value of the field. 0 if the type is not RIO_FLOAT_TYPE
	int intValue;                       // the integer value of the field. 0 if the type is not RIO_INT_TYPE
	bool boolValue;                     // the boolean value of the field. 0 if the type is not RIO_BOOL_TYPE

	bool hasSubFields;                  // a flag to know if the field has subfields
	int nbSubFields;                    // the number of subfields
	struct RIO_VarField** subFields;    // a list of subfields
} RIO_VarField;

/* @function
 * 		Use to initialize a varField structure
 *
 * @param
 *		name :  the name of the var field
 *
 * @return
 * 		Returns a RIO_VarField structure
 */
RIO_VarField* initVarField(const char* name);

/* @function
 * 		Free a field and his subfields recursively
 *
 * @param
 *		fields :  the field to free
 */
void destructVarField(RIO_VarField** fields);

/* @function
 * 		Use to parse a text file into a RIO_VarField
 *
 * @param
 *		path :  the path of the text file
 *
 * @return
 * 		Returns a root RIO_VarField structure containing all the sub fields defined in the text file
 */
RIO_VarField* loadFieldsFromPath(const char* path);

/* @function
 * 		Use to load a file into a given root field. This function could be used but it is preferred
 *      to use loadFieldsFromPath() instead.
 *
 * @param
 *		file :          the file containing the fields information
 *      rootField :     an empty field, initialized with initVarField()
 *
 * @return
 * 		return an error ID depending on what happend
 */
int loadFieldsFromFile(FILE* file, RIO_VarField* rootField);

/* @function
 * 		Count the number of fields defined in a file
 *
 * @param
 *		file :  the file containing the fields information
 *
 * @return
 * 		return an error ID depending on what append
 */
int countFieldInFile(FILE* file);

/* @function
 * 		Print a field and his subField in the terminal
 *
 * @param
 *		rootField :     the root field which will be print with all his subfield
 *      offset :        the number of tabs each line will be offset. This is mainly use because this function is recursive,
 *                      put 0 if you don't know what do put
 */
void printField(RIO_VarField* rootField, int offset);

/* @function
 * 		Use to get a specific subfield from the given field. This function will return the first field corresponding
 *      to the given description the root field
 *
 * @param
 *		fieldTree :     the path where the field is located. The format is : "field1[=value1]/field2[=value2]/.../field"
 *                      for a file looking like :
     *                      #   ...
     *                      #   field1=value1(
     *                      #       ...
     *                      #       field2=value2(
     *                      #           ...
     *                      #           field=value
     *                      #           ...
     *                      #       )
     *                      #       ...
     *                      #   )
     *                      #   ...
 *                      where the "..." could be additional field
 *      rootField :     the root field which will be used for the research. It is usually obtain using loadFieldsFromPath()
 */
RIO_VarField* findField(const char* fieldTree, RIO_VarField* rootField);

/* @function
 * 		Interpret the value of a field based on its stringValue
 *
 * @param
 *		field :     the field to interpret
 */
void interpretFieldValue(RIO_VarField* field);


/* @function
 * 		Check if a string is strictly composed of a given list of chars
 *
 * @param
 *		string : 	the string to be checked
 *		chars : 	the list of char
 *
 * @return
 * 		return true if the string is striclty composed of the character given by the list of chars, false otherwise
 */
bool isStrictlyComposedOf(const char* string, const char* chars);

#endif // RIO_FILE_H
