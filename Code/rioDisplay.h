// General header files
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

// Header file
#include "SDL2/SDL.h"
#include "rioFunction.h"

// This structure is use to control and change the camera
// It is used in all rendering function
// by default the camera as the dimmension of the window
// Please, do not change the value inside the camera without dedicated function
typedef struct RIO_Camera
{
	float x;				// The x coordinate of the camera
	float y;				// The y coordinate of the camera
	float width;			// The width of the camera
	float height;			// The height of the camera
	float windowWidth;		// The width of the window where the camera is used
	float windowHeight;		// The height of the window where the camera is used

	float xScale;			// The scale on the x axis of the camera
	float yScale;			// The scale on the y axis of the camera
	float xOffset;			// The x offset to apply to the coordinate of the cameraView so the camera is correctly centered
	float yOffset;			// The y offset to apply to the coordinate of the cameraView so the camera is correctly centered
} RIO_Camera;

// Use to store multiple camera for multiplayer
typedef struct RIO_ListCamera
{
	RIO_Camera** cameras;	// The different camera
	float xScale;			// The original xScale at which all the cameras as been set
	float yScale;			// The original yScale at which all the cameras as been set
	int size;				// The number of camera stored
} RIO_ListCamera;

/* @function
 * 		Use to initialize a camera structure
 *
 * @param
 *		cameraView : 	the rectangle that define the camera view
 *		windowWidth : 	the width of the window
 *		windowHeight : 	the height of the window
 *
 * @return
 * 		Returns a RIO_Camera structure
 */
RIO_Camera* initCamera(SDL_Rect* cameraView, float windowWidth, float windowHeight);

/* @function
 * 		Use to initialize multiple camera structure so they could be use with multiple screens
 *		Try to not modify the scale and dimmensions of the different cameras in the list
 *
 * @param
 *		nbCamera : 		the number of camera to initialize
 *		xScale : 		how the camera is scaled on the x axis
 *		yScale : 		how the camera is scaled on the y axis
 *						= 1 means no changement
 *						> 1 mean an image zoomed in
 *						< 1 mean an image zoomed out
 *		windowWidth : 	the width of the window
 *		windowHeight : 	the height of the window
 *
 * @return
 * 		Returns a RIO_ListCamera structure. Return NULL if the number of camera is invalid (between 1 and 4 for now)
 */
RIO_ListCamera* initListCamera(int nbCamera, float xScale, float yScale, float windowWidth, float windowHeight);

/* @function
 * 		Use to free a camera from the memory
 *
 * @param
 *		camera : 	the camera we want to free from the memory
 */
void destructCamera(RIO_Camera** camera);

/* @function
 * 		Use to free multiple cameras camera from the memory
 *
 * @param
 *		listCamera : 	the list of camera we want to free from the memory
 */
void destructListCamera(RIO_ListCamera** listCamera);

/* @function
 * 		Use to update the dimmension of the camera
 *
 * @param
 *		listCamera : 	the list of cameras used by the game
 *		xScale : 		how the camera is scaled on the x axis
 *		yScale : 		how the camera is scaled on the y axis
 *						= 1 means no changement
 *						> 1 mean an image zoomed in
 *						< 1 mean an image zoomed out
 *		windowWidth : 	the width of the window
 *		windowHeight : 	the height of the window
 */
void updateCamerasDimmension(RIO_ListCamera* listCamera, float xScale, float yScale, float windowWidth, float windowHeight);

/* @function
 * 		Use to update the dimmension of the camera
 *
 * @param
 *		camera : 		the camera we want to update
 *		newWidth : 		the new width of the camera
 *		newheight : 	the new height of the camera
 *		windowWidth : 	the width of the window
 *		windowHeight : 	the height of the window
 */
void updateCameraDimmension(RIO_Camera* camera, float newWidth, float newHeight, float windowWidth, float windowHeight);

/* @function
 * 		Use to update the SDL_Rect used to draw a texture into a renderer
 *		This will move and scale the rectangle according to the camera
 *
 * @param
 *		camera : 	the camera we will use to update the texture rectangle
 *		tileRect : 	the rectangle use to show a texture on the renderer
 */
void updateTextureRectWithCamera(RIO_Camera* camera, SDL_Rect* tileRect);

/* @function
 * 		Use to move a camera to a given location
 *
 * @param
 *		camera : 			the camera we want to move
 *		x : 				the x coordinate where we want to move
 *		y : 				the y coordinate where we want to move
 *		movementRatio : 	defines the speed at wich the camera will move.
 *							It is a ratio between 0 and 1. 0 it doesn't move, 1 it moves instantaneously
 */
void moveCameraToPoint(RIO_Camera* camera, int x, int y, float movementRatio);

/* @function
 * 		Use to set the renderer view port correctly depending on the screen we want to draw and his corresponding camera
 *
 * @param
 *		listCamera : 			the list of cameras used by the game
 *		idScreen : 				the id of the screen we want to render
 *		renderer : 				the renderer we want to set the view port
 */
void setViewPortScreen(RIO_ListCamera* listCamera, int idScreen, SDL_Renderer* renderer);

/* @function
 * 		Use to get the offset of a camera in case it is snaped to all the other camera so they all create one solid screen
 *
 * @param
 *		camera : 	the list of cameras used by the game
 *		idScreen : 	the id of the screen we want to render
 *		nbScreen : 	the renderer we want to set the view port
 *		offset : 	a list of two elements that will be updated with the offset of the camera
 */
void getCameraOffsetWhenCliped(RIO_Camera* camera, int idScreen, int nbScreen, float* offset);

/* @function
 * 		Use to get the shapes of both camera for a 2 player split screen
 *
 * @param
 *		shapes : 		A pointer to a list of 2 shapes
 *		player1 : 		the center point of the first player
 *		player2 : 		the center point of the second player
 *		windowWidth : 	the width of the window
 *		windowHeight : 	the height of the window
 */
void getCameraShapeTwoPlayer(RIO_Shape** shapes, RIO_Point* player1, RIO_Point* player2, int windowWidth, int windowHeight);

/* @function
 * 		Draw a shape in a renderer
 *
 * @param
 *		renderer : 	the renderer where the shape wil be drawn
 *		shape : 	the shape to draw
 */
void showShape(SDL_Renderer* renderer, RIO_Shape* shape);

/* @function
 * 		Use to toggle the full screen on or off
 *
 * @param
 *		listCamera : 			the list of cameras used by the game
 *		originalScreenWidth : 	the width of the window if it is not full screen
 *		originalScreenHeight : 	the height of the window if it is not full screen
 *		renderer : 				the renderer used by the game
 *		window : 				the  window used by the game
 */
void toggleFullScreen(RIO_ListCamera* listCamera, int originalScreenWidth, int originalScreenHeight, SDL_Renderer* renderer, SDL_Window* window);

