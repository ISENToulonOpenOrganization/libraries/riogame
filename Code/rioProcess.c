
#include "rioProcess.h"

DataType* initData(SDL_Event* event, SDL_Renderer* renderer, SDL_Window* window, int fps, int nbScreen, void (*globalFunction)(struct DataType*, RIO_Event, int))
{
	DataType* data = malloc(sizeof(DataType));
    data->event = event;
    data->renderer = renderer;
    data->window = window;
    data->fps = fps;
	data->frameDuration = 1/(double)(fps);
	data->deltaTime = data->frameDuration;
    data->nbScreen = nbScreen;

    data->globalFunction = globalFunction;

    data->end = false;
    return data;
}

void* inputFct(void* data)
{
	DataType* cdata = (DataType*) data;
	while(!cdata->end)
	{
		SDL_PollEvent(cdata->event);
		(*(cdata->globalFunction))(cdata, RIO_INPUT, -1);
	}
	return 0;
}

void* processFct(void* data)
{
	Uint64 startTime = SDL_GetPerformanceCounter();
	DataType* cdata = (DataType*) data;

	//--- Init Event
	(*(cdata->globalFunction))(cdata, RIO_INIT, -1);

	while(!cdata->end)
	{
		//--- Update DeltaTime
		cdata->deltaTime = ((double)(SDL_GetPerformanceCounter()-startTime)) / (double) SDL_GetPerformanceFrequency();
		startTime = SDL_GetPerformanceCounter();

		//--- Tempo Event
		(*(cdata->globalFunction))(cdata, RIO_TEMPO, -1);

		//--- Graphic Event
		// For each screen, refresh the graphics
		int idScreen;
		for (idScreen = 0; idScreen < cdata->nbScreen; idScreen++)
			(*(cdata->globalFunction))(cdata, RIO_GRAPHIC, idScreen);
		// Refresh the renderer
		SDL_RenderPresent(cdata->renderer);

		//--- Start Delay
		float processTime = ((double)(SDL_GetPerformanceCounter()-startTime)) / SDL_GetPerformanceFrequency();
		if (cdata->frameDuration - processTime > 0)
			SDL_Delay((cdata->frameDuration - processTime)*1000);
	}

	//--- Quit Event
	(*(cdata->globalFunction))(cdata, RIO_QUIT, -1);
	return 0;
}
