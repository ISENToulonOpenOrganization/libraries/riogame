// Header file
#include "SDL2/SDL.h"
#include "rioFunction.h"
#include "rioType.h"
#include "rioFile.h"

// use to define the type of key
typedef enum RIO_TypeKey {UNKNOWN_KEY = -1, LEFT_KEY = 0, UP_KEY, RIGHT_KEY, DOWN_KEY, ATTACK_KEY, NB_KEY} RIO_TypeKey;

// This structure contains a flag for each important key on the keyboard
// The flag is at true if the key is pressed and false if it is not
typedef struct RIO_KeyboardInput
{
	bool keyPressed[NB_PLAYER][NB_KEY];		// The flags for the keys
	int keySDLRef[NB_PLAYER][NB_KEY];		// the SDLK for the keys
}RIO_KeyboardInput;

/* @function
 * 		Use to allocate the memory for a Rio_KeyboardInput structure
 *
 * @param
 *
 *
 * @return
 * 		Returns a Rio_KeyboardInput with all the flag set at false
 */
RIO_KeyboardInput* initKeyboardInput();

/* @function
 * 		Use to free a Rio_KeyboardInput structure from the memory
 *
 * @param
 *		keyboardInput : 	the Rio_KeyboardInput structure we want to free
 */
void destructKeyboardInput(RIO_KeyboardInput** keyboardInput);

/* @function
 * 		Use to update the flags in a Rio_KeyboardInput structure
 *
 * @param
 *		keyboardInput : 	a Rio_KeyboardInput structure containing different input flags
 *		event : 			an SDL_Event use to know what event is currently happening
 */
void updateKeyboardInput(RIO_KeyboardInput* keyboardInput, SDL_Event* event);

/* @function
 * 		Use to load a keyboard mapping from a file
 *
 * @param
 *		keyboardInput : 		the keyboardInput where the data will be load.
 *		pathKeyboardInput : 	the path of the file where the keyboard input info are stored
 *
 * @return
 *		return an error ID depending on what happend
 */
int loadKeyboardInput(RIO_KeyboardInput** keyboardInput, char* pathKeyboardInput);

/* @function
 * 		converte a string to a SDL_Keycode value
 *		
 * @param
 *		str : 	the string to converte
 *
 * @return
 *		return a RIO_TypePath
 */
SDL_Keycode strtoSDLK(char* str);

/* @function
 * 		converte a string to a RIO_TypeKey
 *		
 * @param
 *		str : 	the string to converte
 *
 * @return
 *		return a RIO_TypeKey
 */
RIO_TypeKey strtoTypeKey(char* str);

