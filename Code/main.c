
// Header file
#include "rioProcess.h"
#include "rioEntity.h"
#include "rioFile.h"

#define WINDOW_WIDTH 960
#define WINDOW_HEIGHT 540
#define FRAME_PER_SECOND 60
#define NB_SCREEN 2
#define PROGRAM_NAME "Kevin's Quest"

////////////////////////
//--- main program ---//
////////////////////////

// use to code the main program, will be called regularly by threads
void globalFunction(DataType* data, RIO_Event globalEvent, int idScreen)
{
	// Temp, to delete later
	static SDL_Color col1 = {0, 200, 200, 120};
	static SDL_Color col2 = {200, 200, 0, 120};
	static SDL_Color col3 = {200, 0, 0, 120};

	// Use to store the SDL renderer and event
	static SDL_Renderer* renderer = NULL;
	static SDL_Window* window = NULL;
	static SDL_Event* event = NULL;
	// Use to store the cameras
	static RIO_ListCamera* listCamera = NULL;
	// Use to store the important inputs from the keyboard
	static RIO_KeyboardInput* keyboardInput = NULL;

	// The color we use as transparency for the tilesets
	static SDL_Color transparencyColor = {88, 232, 212, 255};

	// Use to store the entity of the players
	static RIO_Entity* entity_player1 = NULL;
	static RIO_Entity* entity_player2 = NULL;
	// Use to store all the entities
	static RIO_ListEntity* listEntity = NULL;

	// Use to store the texture of all tiles in the tileset of the map
	static RIO_TileSetMap* tileSetMap = NULL;
	// Use to store the map
	static RIO_Map* map = NULL;

	// Use to store the size of a tile
	static int tileSize = 16;

	static RIO_Shape* shapes[2];

	switch(globalEvent)
	{
		case RIO_INIT: // Called one time, when the program start
			// We set the renderer and the window
			renderer = data->renderer;
			window = data->window;
			// We set the event
			event = data->event;
			// We set the inputs
			loadKeyboardInput(&keyboardInput, "option/keyboardMapping.txt");

			// We initialize the cameras
			listCamera = initListCamera(data->nbScreen, 3, 3, WINDOW_WIDTH, WINDOW_HEIGHT);

			// We load all the textures of the map into a tileset structure
			tileSetMap = createTileSetMap(renderer, tileSize, transparencyColor, "map/infoTileForest.txt");
			// We initialize the map
			map = initMap(tileSetMap, "map/map.txt");

			// We load all the textures of the players into a tileset structure
			loadEntity(&entity_player1, "entity/player1Info.txt", renderer, transparencyColor);
			loadEntity(&entity_player2, "entity/player2Info.txt", renderer, transparencyColor);

			// We initialize the list of entities
			listEntity = initListEntity(20);
			// We add the players to the list of entities
			listEntity->entities[0] = entity_player1;
			listEntity->entities[1] = entity_player2;
			// We create an immovable crate
			loadEntity(&listEntity->entities[2], "entity/blockInfo.txt", renderer, transparencyColor);
			getCompleteEntityPathToCoord(listEntity->entities[2], listEntity->entities[2]->x, listEntity->entities[2]->y + 20, map);
			listEntity->entities[2]->pathType = BACK_AND_FORTH;
			// We create an immovable crate
			loadEntity(&listEntity->entities[7], "entity/blockInfo.txt", renderer, transparencyColor);
			listEntity->entities[7]->x = 250;
			listEntity->entities[7]->y = 240;
			getCompleteEntityPathToCoord(listEntity->entities[7], listEntity->entities[7]->x + 50, listEntity->entities[7]->y, map);
			listEntity->entities[7]->pathType = BACK_AND_FORTH;
			// We create a movable crate
			loadEntity(&listEntity->entities[3], "entity/crateInfo.txt", renderer, transparencyColor);
			listEntity->entities[3]->x = 250;
			listEntity->entities[3]->y = 120;
			// We add a dummy enemy entity
			loadEntity(&listEntity->entities[4], "entity/enemy2Info.txt", renderer, transparencyColor);
			listEntity->entities[4]->x = 260;
			listEntity->entities[4]->y = 116;
			// We add a dummy enemy entity
			loadEntity(&listEntity->entities[5], "entity/enemyInfo.txt", renderer, transparencyColor);
			listEntity->entities[5]->x = 310;
			listEntity->entities[5]->y = 116;
			getCompleteEntityPathToCoord(listEntity->entities[5], 390, 144, map);
			listEntity->entities[5]->pathType = LOOP;
			// We add a dummy enemy entity
			loadEntity(&listEntity->entities[6], "entity/enemyInfo.txt", renderer, transparencyColor);
			listEntity->entities[6]->x = 210;
			listEntity->entities[6]->y = 166;
			getCompleteEntityPathToCoord(listEntity->entities[6], 280, 196, map);
			listEntity->entities[6]->pathType = BACK_AND_FORTH;
 			break;

		case RIO_TEMPO: // Called at each frames
			// We update the movement of the players according to the keyboard inputs
			updateStateEntityWithInputs(entity_player1, keyboardInput, PLAYER1);
			updateStateEntityWithInputs(entity_player2, keyboardInput, PLAYER2);
			followStaticPath(listEntity->entities[2], listEntity->entities[2]->maxSpeed);
			followStaticPath(listEntity->entities[7], listEntity->entities[7]->maxSpeed);
			// We make one enemy follow a path to the first player
			getCompleteEntityPathToEntity(listEntity->entities[4], entity_player1, map);
			followDynamicPath(listEntity->entities[4], 20);
			// We make one enemy follow a path in loop
			followStaticPath(listEntity->entities[5], 20);
			// We make one enemy follow a path back and forth
			followStaticPath(listEntity->entities[6], 1);

			// We update the position of all the entities based on their speed
			updatePositionEntities(listEntity, map);
			// We update the animation of all entities
			updateAnimationEntities(listEntity, data->fps);

			// We move the cameras in the direction of the players
			moveMultiPlayerCameras(listCamera, 0.2, entity_player1, entity_player2, NULL, NULL);

			break;

		case RIO_GRAPHIC: // Called at each frames

			// We set the viewPort (part of the screen to update) according to the camera that is currently being rendered
			//	this is usefull only with multiplayer.
			setViewPortScreen(listCamera, idScreen, renderer);

			// We draw the map in the renderer
			showMap(map, renderer, listCamera->cameras[idScreen]);
			// We draw all the entities
			showEntities(listEntity, renderer, listCamera->cameras[idScreen]);
			showHealthEntities(listEntity, PLAYER, renderer, listCamera->cameras[idScreen]);

			// We draw the collision boxes
			//showCollisionBoxEntities(listEntity, renderer, listCamera->cameras[idScreen]);
			//showAttackCollisionBoxEntity(entity_player1, renderer, listCamera->cameras[idScreen]);
			//showAttackCollisionBoxEntity(entity_player2, renderer, listCamera->cameras[idScreen]);

			break;

		case RIO_INPUT: // Called when a SDL_Event happens
			// We update the important keyboard input flags
			updateKeyboardInput(keyboardInput, event);

			// Depending on the SDL event type
			switch(event->type)
			{
				// When a key is pressed down
				case SDL_KEYDOWN:
					// Depending on the special key pressed
					switch(event->key.keysym.sym)
					{
						// When the escape key is pressed
						case SDLK_ESCAPE:
							// We end the program
							data->end = true;
							break;
						// When the F key is pressed
						case SDLK_f:
							// We toggle the full screen
							toggleFullScreen(listCamera, WINDOW_WIDTH, WINDOW_HEIGHT, renderer, window);
							break;
					}
					break;
				// When we click on the red cross in the top right corner
				case SDL_QUIT:
					// We end the program
					data->end = true;
					break;
			}
			break;

		case RIO_QUIT: // Called when the program end (when data->end is set to true)
			// We free the list of entities from the memory (after we free the tileSet)
			destructListEntity(&listEntity);

			// We free the tileset of the map from the memory
			destructTileSetMap(&tileSetMap);
			// We free the map from the memory (after we free the tileSet)
			destructMap(&map);

			// We free the cameras from the memory
			destructListCamera(&listCamera);

			// We free the keyboard flags from the memory
			destructKeyboardInput(&keyboardInput);
			break;
	}
}

//////////////////////
//--- setting up ---//
//////////////////////

int main(int arc, char *argv[])
{
	printf("\tProgram start\n");
	// Structures use by SDL
	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_Event event;
	// Variable status, use to know if an error occurred
	int statut = EXIT_FAILURE;

	printf("Initialization of SDL, window and renderer\n");
	// Initialization of the SDL
	if(SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		fprintf(stderr, "Error SDL_Init : %s", SDL_GetError());
		goto Quit;
	}
	// Initialization of the window
	window = SDL_CreateWindow(PROGRAM_NAME, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		                      WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
	if(window == NULL)
	{
		fprintf(stderr, "Erreur SDL_CreateWindow : %s", SDL_GetError());
		goto Quit;
	}
	// Initialization of the renderer
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if(renderer == NULL)
	{
		fprintf(stderr, "Erreur SDL_CreateRenderer : %s", SDL_GetError());
		goto Quit;
	}

	// We set the renderer so he can use transparent textures
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	// Set the seed for the random
	srand(123456);

	printf("Setting up the threads\n");
	// Declaration of the data used in all threads
	DataType* data = initData(&event, renderer, window, FRAME_PER_SECOND, NB_SCREEN, &globalFunction);

    // Start of the thread for the processes
    SDL_Thread* thread1 = SDL_CreateThread((SDL_ThreadFunction) processFct, "Process Thread", data);

	printf("Program running...\n");
    // Start of the events handling
	inputFct((void*) data);

	// Once the end flag as been trigger
	printf("Waiting for end of threads\n");
    // We wait for the end of all threads
    SDL_WaitThread(thread1, NULL);


	printf("Closing window\n");
	// we close the window
	Quit:
		if(NULL != renderer)
			SDL_DestroyRenderer(renderer);
		if(NULL != window)
			SDL_DestroyWindow(window);
		SDL_Quit();
		return statut;
}


