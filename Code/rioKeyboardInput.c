#include "rioKeyboardInput.h"

RIO_KeyboardInput* initKeyboardInput()
{
	// We allocate the memory for the structure
	RIO_KeyboardInput* keyboardInput = (RIO_KeyboardInput*) malloc(sizeof(RIO_KeyboardInput));
	// We set all the flags and the SLD reference
	int indexPlayer, indexKey;
	for (indexPlayer = 0; indexPlayer < NB_PLAYER; indexPlayer++)
	{
		for (indexKey = 0; indexKey < NB_KEY; indexKey++)
		{
			keyboardInput->keyPressed[indexPlayer][indexKey] = false;
			keyboardInput->keySDLRef[indexPlayer][indexKey] = SDLK_UNKNOWN;
		}
	}
	// Then, we return it
	return keyboardInput;
}

void destructKeyboardInput(RIO_KeyboardInput** keyboardInput)
{
	// If the pointer exists
	if (keyboardInput != NULL)
	{
		// If what he points for exists
		if (*keyboardInput != NULL)
		{
			// Free the keyboardInput from the memory
			free(*keyboardInput);
			*keyboardInput = NULL;
		}
	}
}

void updateKeyboardInput(RIO_KeyboardInput* keyboardInput, SDL_Event* event)
{
	// For each player
	int indexPlayer, indexKey;
	for (indexPlayer = 0; indexPlayer < NB_PLAYER; indexPlayer++)
	{
		for (indexKey = 0; indexKey < NB_KEY; indexKey++)
		{
			if (event->type == SDL_KEYDOWN)
			{
				if (event->key.keysym.sym == keyboardInput->keySDLRef[indexPlayer][indexKey])
					keyboardInput->keyPressed[indexPlayer][indexKey] = true;
			}
			if (event->type == SDL_KEYUP)
			{
				if (event->key.keysym.sym == keyboardInput->keySDLRef[indexPlayer][indexKey])
					keyboardInput->keyPressed[indexPlayer][indexKey] = false;
			}
		}
	}
}

int loadKeyboardInput(RIO_KeyboardInput** keyboardInput, char* pathKeyboardInput)
{
	int error = RIO_ERROR_NONE; // use to keep track of the error
	if (pathKeyboardInput != NULL)
	{
		if (*keyboardInput == NULL)
		{
			*keyboardInput = initKeyboardInput();
		}
		
    	// Read the given file
    	RIO_VarField* keyboardInputRootFields = loadFieldsFromPath(pathKeyboardInput);
    	// If the root is defined correctly
        if (keyboardInputRootFields != NULL &&															// Exists
            keyboardInputRootFields->hasSubFields && keyboardInputRootFields->subFields != NULL)		// has subfields
        {
			// For each player
			int id_player;
            for(id_player = 1; id_player <= NB_PLAYER; id_player++)
            {
            	// Find his keyboard mapping
            	char fieldName[32];
            	sprintf(fieldName, "controlKey=P%d", id_player);
            	RIO_VarField* currentPlayerField = findField(fieldName, keyboardInputRootFields);
            	// If the mapping is correctly defined
            	if (currentPlayerField != NULL && currentPlayerField->hasSubFields && currentPlayerField->subFields != NULL)
            	{
            		// For each subfield (key mapping)
            		int i;
            		for(i = 0; i < currentPlayerField->nbSubFields; i++)
            		{
            			RIO_VarField* keyfield = currentPlayerField->subFields[i];
            			// Get the name of the key
            			RIO_TypeKey indexKey = strtoTypeKey(keyfield->name);
            			// If it correspond to an existing key in the game
    					if (keyfield != NULL && indexKey != UNKNOWN_KEY)
    						// We set it in the keyboard input structure
        					(*keyboardInput)->keySDLRef[id_player-1][indexKey] = strtoSDLK(keyfield->stringValue);
        			}
            	}
            }
        }
    	
    }
    else
    	error = RIO_ERROR_NULLFILE;
    return error;
}

/*int loadKeyboardInput(RIO_KeyboardInput** keyboardInput, char* pathKeyboardInput)
{
	int error = RIO_ERROR_NONE; // use to keep track of the error
	if (*keyboardInput == NULL)
	{
		*keyboardInput = initKeyboardInput();
	}
	FILE* file; // use to store the file where the map data is stored
	// If we managed to open the file
	if((file = fopen(pathKeyboardInput, "r")))
	{
		char currentCharacter; // use to store the characters we will read

		//////////////////////////////
		//--- read the tile info ---//
		//////////////////////////////

		// We create some string buffer
		char strVariable[200] = "";
		int sizeStrVariable = 0;
		char strValue[200] = "";
		int sizeStrValue = 0;
		bool isInParenthesis = false;
		bool isReadingValues = false;
		int valueIndex = 0;
		// We create a string buffer to convert the character to a string
		char strChar[2] = "\0";

		RIO_TypePlayer indexPlayer = PLAYER1;
		RIO_TypeKey indexKey = LEFT_KEY;

		do
		{
			// We read a character
			currentCharacter = fgetc(file);
			// This is the character that indicate the end of a line
			if (currentCharacter == ';' || currentCharacter == ',' || currentCharacter == ')')
			{
				// If we are currently on the details of a value
				if (isInParenthesis)
				{
					indexKey = atoTypeKey(strVariable);
					if (indexKey != UNKNOWN_KEY)
					{
						// Depending on the sub value
						switch(valueIndex)
						{
							// This is the type of player
							case 0:
								indexPlayer = atoTypePlayer(strValue);
								break;
							// This is the key
							case 1:
								(*keyboardInput)->keySDLRef[indexPlayer][indexKey] = atoSDLK(strValue);
								break;
						}
					}
					// We reset the sub value we were reading
					strcpy(strValue, "");
					sizeStrValue = 0;
					// We go to the next sub value
					valueIndex++;

					if (currentCharacter == ')')
					{
						strcpy(strValue, "");
						sizeStrValue = 0;
						strcpy(strVariable, "");
						sizeStrVariable = 0;
						isInParenthesis = false;
					}
				}
				else
				{
					// We reset everything
					strcpy(strValue, "");
					sizeStrValue = 0;
					strcpy(strVariable, "");
					sizeStrVariable = 0;
					// We say that we're not reading a value anymore (means that we will read a variable)
					isReadingValues = false;
				}
			}
			// This is the character which means that we will read the value of a variable
			else if (currentCharacter == '=')
			{
				isReadingValues = true;
			}
			// This is the character which means that the variable have multiple values (caleld sub values)
			else if (currentCharacter == '(')
			{
				valueIndex = 0;
				isInParenthesis = true;
			}
			// These are character that are used to make the file more visible (therefor, they are ignored)
			else if (currentCharacter != '\n' && currentCharacter != '\t' && currentCharacter != '\"' && currentCharacter != ' ')
			{
				if (isReadingValues)
				{
					if (sizeStrValue < 199)
					{
						strChar[0] = currentCharacter;
						sizeStrValue++;
						strcat(strValue, strChar);
					}
				}
				else
				{
					if (sizeStrVariable < 199)
					{
						strChar[0] = currentCharacter;
						sizeStrVariable++;
						strcat(strVariable, strChar);
					}
				}
			}
		} while (currentCharacter != EOF); // While the character we read is valid
		// We close the file
		fclose(file);
	}
	else
	{
		// We update the error
		error = RIO_ERROR_READ;
	}
	return error;
}*/

SDL_Keycode strtoSDLK(char* str)
{
	int len = strlen(str);
	// if the lenght of the string is too low
	if (len < 1)
	{
		return SDLK_UNKNOWN;
	}

	// If char or number
	if (len == 1)
	{
		// We convert the str to a number
		char* endptr;
		int value = strtol(str, &endptr, 10);
		// If it is not a number
		if (value == 0 && endptr == str)
		{
			// If it is an upper case letter
			if (65 <= (int) str[0] && (int) str[0] <= 90)
				// conversion to an SDLK
				return (int) str[0] + 32;
			// If it is a lower case letter
			if (97 <= (int) str[0] && (int) str[0] <= 122)
				// conversion to an SDLK
				return (int) str[0];
			// If it's something else
			else
			{
				if (strcmp(str, "!") == 0)
					return SDLK_EXCLAIM;
				else if (strcmp(str, ":") == 0)
					return SDLK_COLON;
				else if (strcmp(str, ";") == 0)
					return SDLK_SEMICOLON;
				else if (strcmp(str, ",") == 0)
					return SDLK_COMMA;
				else if (strcmp(str, "*") == 0)
					return SDLK_ASTERISK;
				else if (strcmp(str, "$") == 0)
					return SDLK_DOLLAR;
				else if (strcmp(str, "<") == 0)
					return SDLK_LESS;
				else
					return SDLK_UNKNOWN;
			}
		}
		// If it is a number
		else
		{
			if (value == 0)
				return SDLK_KP_0;
			else
				// conversion to an SDLK
				return value + 1073741912;
		}

	}
	// If other kind of key
	else
	{
		if (strcmp(str, "UP") == 0)
			return SDLK_UP;
		else if (strcmp(str, "RIGHT") == 0)
			return SDLK_RIGHT;
		else if (strcmp(str, "DOWN") == 0)
			return SDLK_DOWN;
		else if (strcmp(str, "LEFT") == 0)
			return SDLK_LEFT;
		else if (strcmp(str, "SPACE") == 0)
			return SDLK_SPACE;
		else
			return SDLK_UNKNOWN;
	}
}

RIO_TypeKey strtoTypeKey(char* str)
{
	if (strcmp(str, "left") == 0)
		return LEFT_KEY;
	else if (strcmp(str, "up") == 0)
		return UP_KEY;
	else if (strcmp(str, "right") == 0)
		return RIGHT_KEY;
	else if (strcmp(str, "down") == 0)
		return DOWN_KEY;
else if (strcmp(str, "attack") == 0)
		return ATTACK_KEY;
	else
		return UNKNOWN_KEY;
}
