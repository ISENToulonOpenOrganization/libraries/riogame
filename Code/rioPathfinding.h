//General header files
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//More specific headers files
#include <SDL2/SDL.h>
#include <math.h>
#include <stdbool.h>

//Header file
#include "rioMap.h"

//The structure node used in the A* algorithme
typedef struct node 
{
	int x;
	int y;
	int cost;
	int heuristic;
	struct node* linkedNode;
}node;

//Initialisation and destruction function
node* initNode(int x, int y, int cost, int heuristic);
void destructNodes(node** frontNode);
void setHeuristic(node* nodeToSet, node* endNode);

//Manipulation function
node* popNode(node** frontNode);
void rmvNode(node** frontNode, node* nodeToRemove);
node* cpyNode(node* nodeToCpy);
void insertFrontNode(node** frontNode, node* newNode);

//Information function
void viewNodes(node** frontNode, SDL_Renderer* renderer, RIO_Camera* camera, SDL_Color col, int tileSize);
int nodeCmp(node* n1, node* n2);
int distNodes(node* n1, node* n2);
bool isCoordInSet(node** frontNode, int x, int y);
bool isNodeInSet(node** frontNode, node* nodeToCheck);
bool isNextTo(node* nodeToCheck, int x, int y);
node* getLastNode(node** frontNode);

//A* algorithme function
node* getLowestNode(node** openSet);
node* getPath(node** closedSet, node* endNode);
void addNeighbors(node** openSet, node** closedSet, node* currentNode, node* endNode, RIO_Map* map);
node* AStar(node** openSet, node** closedSet, node* startNode, node* endNode, RIO_Map* map);

//For real coordinate (not tile coordiante)
void updatePathToCoordinate(node** frontNode, int tileSize);
node* getNodeAfter(node** path, node* indexNode);
node* getNodeBefore(node** path, node* indexNode);
node* getClosetNodeToCoord(node** path, int x, int y);
node* getReversePath(node** path);

