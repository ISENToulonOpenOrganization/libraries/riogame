
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <time.h>
#include <math.h>
#include <stdbool.h>
#include <pthread.h>
#include <SDL2/SDL.h>

// global event info used by the globalFunction
typedef enum{RIO_INIT = 0, RIO_TEMPO, RIO_GRAPHIC, RIO_INPUT, RIO_QUIT} RIO_Event;

// This structure is used in the inputFct function to be thread
// It contains every variables which needed to be use in the main and inputFct function
typedef struct DataType
{
	SDL_Event* event; // This is the structure event use by SDL
	SDL_Renderer* renderer; // This is the structure renderer use by SDL
	SDL_Window* window; // This is the structure window use by SDL
	int fps; // This is the variable that defines the number of frame per second
	double frameDuration; // This is the variable that defines the time a frame takes, in second
	double deltaTime; // This is the variable that defines the current deltaTime
	int nbScreen; // This is the variable that define the number of screens showed in the window

	void (*globalFunction)(struct DataType*, RIO_Event, int);

	bool end; // This is the variable use to know when to end the function
} DataType;

// Initialisation function
DataType* initData(SDL_Event* event, SDL_Renderer* renderer, SDL_Window* window, int fps, int nbScreen, void (*globalFunction)(struct DataType*, RIO_Event, int));

// This function must be used in a thread. It constantly update the event structure until "data->end" is true
// It's role is to call the case RIO_INPUT constantly
void* inputFct(void* data);

// This function must be used in a thread. It constantly update the event structure until "data->end" is true
// It's role is to call the case RIO_TEMPO and RIO_GRAPHIC each frame
void* processFct(void* data);
