#include "rioMap.h"

RIO_Map* initMap(RIO_TileSetMap* tileSet, char* path)
{
 	RIO_Map* map = NULL; // Use to store the Rio_Map structure
 	// If all the arguments are valid
 	if (tileSet != NULL)
 	{
 		// We allocate the memory to store the map
 		map = (RIO_Map*) malloc(sizeof(RIO_Map));
 		// We update his height and width
 		map->height = 0;
 		map->width = 0;
 		// we set the map as NULL
 		map->mapData = NULL;
 		// We update the tileset of the map
 		map->tileSet = tileSet;

 		// If a path as been given
 		if (path != NULL)
 		{
 			// We try to load it
 			int error = loadMap(map, path);
 			// Depending on the error
 			switch (error)
 			{
 				case RIO_ERROR_READ:
 					printf("Error, can't read the map\n");
 					break;
 				case RIO_ERROR_SIZE:
 					printf("Error, the file is invalid\n");
 					break;
 			}
 		}
 	}
 	// Then, we return the map
 	return map;
}

void destructMap(RIO_Map** map)
{
	// If the pointer exists
 	if (map != NULL)
 	{
 		// If what he points for exists
 		if (*map != NULL)
 		{
 			// We free the data of the map from the memory
 			destructMatrice2D((void**) (*map)->mapData, (*map)->width);
 			// Free the map from the memory
 			free(*map);
 			*map = NULL;
 		}
 	}
}

void showMap(RIO_Map* map, SDL_Renderer* renderer, RIO_Camera* camera)
{
	// We create a rectangle which will be use to render the texture of each tiles
	SDL_Rect tileRect = {0, 0, map->tileSet->tileSize, map->tileSet->tileSize};
	// We create a rectangle which will be use to know if the texture we want to render is visible on the window
	SDL_Rect windowRect = {0, 0, camera->windowWidth, camera->windowHeight};
	// For each column
	int col, line;
	for (col = 0; col < map->width; col++)
	{
		// For each line
		for (line = 0; line < map->height; line++)
		{
			// We load a default texture
			SDL_Texture* textureTemp = map->tileSet->tilesInfo[0]->listTexture[0];
			// We update the coordinate and the size of the rectangle which will draw the tile
			tileRect.x = col * map->tileSet->tileSize;
			tileRect.y = line * map->tileSet->tileSize;
			tileRect.w = map->tileSet->tileSize;
			tileRect.h = map->tileSet->tileSize;
			// We update the rectangle with the camera
			updateTextureRectWithCamera(camera, &tileRect);

			// For each tiles in the tiles info of the tileSet
			int tileIndex;
			for(tileIndex = 0; tileIndex < map->tileSet->nbTileInfo; tileIndex++)
			{
				// If the tile info is not NULL
				if (map->tileSet->tilesInfo[tileIndex] != NULL)
				{
					// If the ID of the tile info is equal to the one on the map data
					if (map->tileSet->tilesInfo[tileIndex]->id == map->mapData[col][line])
					{
						// If it's an auto tile
						if (map->tileSet->tilesInfo[tileIndex]->isAutoTile)
						{
							int textureIndex = 0;
							// Top right
							if (line-1 >= 0 && col+1 < map->width)
							{
								if (map->mapData[col][line-1] == map->tileSet->tilesInfo[tileIndex]->id &&
									map->mapData[col+1][line] == map->tileSet->tilesInfo[tileIndex]->id &&
									map->mapData[col+1][line-1] == map->tileSet->tilesInfo[tileIndex]->id)
									textureIndex |= 1<<0;
							}
							// bottom right
							if (col+1 < map->width && line+1 < map->height)
							{
								if (map->mapData[col][line+1] == map->tileSet->tilesInfo[tileIndex]->id &&
									map->mapData[col+1][line] == map->tileSet->tilesInfo[tileIndex]->id &&
									map->mapData[col+1][line+1] == map->tileSet->tilesInfo[tileIndex]->id)
									textureIndex |= 1<<1;
							}
							// bottom left
							if (line+1 < map->height && col-1 >= 0)
							{
								if (map->mapData[col][line+1] == map->tileSet->tilesInfo[tileIndex]->id &&
									map->mapData[col-1][line] == map->tileSet->tilesInfo[tileIndex]->id &&
									map->mapData[col-1][line+1] == map->tileSet->tilesInfo[tileIndex]->id)
									textureIndex |= 1<<2;
							}
							// Top left
							if (col-1 >= 0 && line-1 >= 0)
							{
								if (map->mapData[col][line-1] == map->tileSet->tilesInfo[tileIndex]->id &&
									map->mapData[col-1][line] == map->tileSet->tilesInfo[tileIndex]->id &&
									map->mapData[col-1][line-1] == map->tileSet->tilesInfo[tileIndex]->id)
									textureIndex |= 1<<3;
							}
							// We load the texture
							textureTemp = map->tileSet->tilesInfo[tileIndex]->listTexture[textureIndex];
						}
						else
						{
							// We load the texture
							textureTemp = map->tileSet->tilesInfo[tileIndex]->listTexture[0];
						}
					}
				}
			}

			// If the texture is visible on the window
			if (SDL_HasIntersection(&tileRect, &windowRect))
			{
				// Draw the texture of the tile
				SDL_RenderCopy(renderer, textureTemp, NULL, &tileRect);
			}
		}
	}
}

int loadMap(RIO_Map* map, char* path)
{
	int error = RIO_ERROR_NONE; // use to keep track of the error
	FILE* file; // use to store the file where the map data is stored
	// If we managed to open the file
	if((file = fopen(path, "r")))
	{
		// Will be use to store the height and width of the map
		int mapWidth = 0;
		int mapHeight = 0;
		char currentCharacter; // use to store the characters we will read

		// Check for the height of the map
		do
		{
			// We read a character
			currentCharacter = fgetc(file);
			// Each time the character is a cariage return, we increase the height of the map
			if (currentCharacter == '\n')
				mapHeight++;

		} while (currentCharacter != EOF); // While the character we read is valid

		// We go back to the beginning of the file
		rewind(file);

		// Check for the width of the map
		int currentLineWidth = 0; // use to store the width of the current line
		int currentLine = 0; // use to know at wich line we're currently on
		do
		{
			// We read a character
			currentCharacter = fgetc(file);
			// If the character is not invalid
			if (currentCharacter == ' ' || currentCharacter == '\n')
				// We increase the width of the line we're currently on
				currentLineWidth++;
			// If the character is a cariage return
			if (currentCharacter == '\n')
			{
				// We increase update the current line we're on
				currentLine++;
				// If we we're not on the first line
				if (currentLine != 1)
				{
					// If the line we're on is not the same length as the previous one
					if (currentLineWidth != mapWidth)
					{
						// Then, the map data is not valid, so we call an error
						error = RIO_ERROR_SIZE;
					}
				}
				// We update the width of the map with the width of the current line
				mapWidth = currentLineWidth;
				// We reset the width of the current line
				currentLineWidth = 0;
			}

		} while (currentCharacter != EOF); // While the character we read is valid

		// If the width or hight is too short
		if (mapWidth <= 0 || mapHeight <= 0)
		{
			// Then, the map data is not valid, so we call an error
			error = RIO_ERROR_SIZE;
		}

		// If no error as been detected
		if (error == RIO_ERROR_NONE)
		{
			// If their is already a data map loaded
			if (map->mapData != NULL)
			{
				// We free the data of the map from the memory
 				destructMatrice2D((void**) map->mapData, map->width);
 				map->mapData = NULL;
			}
			// We update the hight and width of the map
			map->height = mapHeight;
			map->width = mapWidth;
			// We allocate the memory for the map data
			map->mapData = (int**) malloc(sizeof(int*) * mapWidth);
			// For each column of the map data
			int col, line;
			for(col = 0; col < mapWidth; col++)
			{
				// We allocate the line of map data
				map->mapData[col] = (int*) malloc(sizeof(int) * mapHeight);
			}

			// We go back to the beginning of the file
			rewind(file);
			// We create a string buffer to store the value in the map file
			char strBuffer[20] = "";
			// We create a string buffer to convert the character to a string
			char strChar[2] = "\0";

			// we reset the column and line index
			col = 0;
			line = 0;
			// For each character in the file
			do
			{
				// We read a character
				currentCharacter = fgetc(file);
				// If it is not a special character
				if (currentCharacter != ' ' && currentCharacter != '\n')
				{
					// We convert the character to a string
					strChar[0] = currentCharacter;
					// We add the character to the buffer
					strcat(strBuffer, strChar);
				}
				// If the column and line index are valid
				else if (col < map->width && line < map->height)
				{
					// We update the map with the value of the tile in the file
					map->mapData[col][line] = atoi(strBuffer);
					// We increase the column index
					col++;
					// We reset the string buffer
					strcpy(strBuffer, "");
				}
				// Each time the character is a cariage return, we increase the height of the map
				if (currentCharacter == '\n')
				{
					// We reset the column index
					col = 0;
					// We increase the line index
					line++;
				}
			} while (currentCharacter != EOF); // While the character we read is valid
		}

		// We close the file
		fclose(file);
	}
	else
	{
		// We update the error
		error = RIO_ERROR_READ;
	}
	return error;
}
