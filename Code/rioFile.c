#include "rioFile.h"

RIO_VarField* initVarField(const char* name)
{
    RIO_VarField* varField = (RIO_VarField*) malloc(sizeof(RIO_VarField));
    strcpy(varField->name, name);
    varField->type = RIO_UNKNOWN_TYPE;

    strcpy(varField->stringValue, "");
    varField->floatValue = 0;
    varField->intValue = 0;
    varField->boolValue = false;

    varField->hasSubFields = false;
	varField->nbSubFields = 0;
	varField->subFields = NULL;
	return varField;
}

void destructVarField(RIO_VarField** fields)
{
    if (fields != NULL)
    {
        if (*fields != NULL)
        {
            if ((*fields)->subFields != NULL)
            {
                int i;
                for (i = 0; i < (*fields)->nbSubFields; i++)
                    destructVarField(&((*fields)->subFields[i]));
                free((*fields)->subFields);
                free(*fields);
            }
        }
    }
}

RIO_VarField* loadFieldsFromPath(const char* path)
{
	int error = RIO_ERROR_NONE;
	RIO_VarField* rootField = NULL;
	if (path != NULL)
	{
    	rootField = initVarField("root");
    	rootField->type = RIO_FIELD_TYPE;
    	FILE* file = fopen(path, "rb");
    	error = loadFieldsFromFile(file, rootField);
    	if (file != NULL)
    		fclose(file);
    }
    else
    	error = RIO_ERROR_NULLFILE;
    	
    switch (error)
 	{
 		case RIO_ERROR_READ:
 			printf("%sError%s: the file \"%s\" can't be read\n",
 			BOLDRED_COLOR_TER,
 			RESET_COLOR_TER,
 			path);
 			break;
 		case RIO_ERROR_NULLFILE:
 			printf("%sError%s: can't read a file with a NULL path\n",
 			BOLDRED_COLOR_TER,
 			RESET_COLOR_TER);
 			break;
 	}
 	
    return rootField;
}

int loadFieldsFromFile(FILE* file, RIO_VarField* rootField)
{
	int error = RIO_ERROR_NONE; // use to keep track of the error
	// If we managed to open the file
	if(file != NULL && rootField != NULL)
	{
	    // Check the number of subField
        int nbSubFields = countFieldInFile(file);
        // Go further only if subFields have been found
        if (nbSubFields > 0)
        {
            // Initialize the subFields
            rootField->hasSubFields = true;
            rootField->type = RIO_FIELD_TYPE;
            rootField->nbSubFields = nbSubFields;
            rootField->subFields = (RIO_VarField**) malloc(sizeof(RIO_VarField*) * nbSubFields);
            int i;
            for(i = 0; i < nbSubFields; i++)
                rootField->subFields[i] = initVarField("");

            // Keep track of current field
            int subFieldIndex = 0;
            RIO_VarField* currentField = rootField->subFields[subFieldIndex];

            // String buffer to read the values
            char strValue[256] = "";
            int sizeStrValue = 0;
            // String buffer to convert the character to a string
            char strChar[2] = "\0";

            RIO_ReadingState readState = RIO_READ_NAME;
            char currentChar; // use to store the characters we will read
            do
            {
                currentChar = fgetc(file);
                // Next is the beginning of the value
                if (currentChar == '=')
                {
                    if (readState == RIO_READ_NAME)
                        strcpy(currentField->name, strValue);
                    else
                        error = RIO_ERROR_FILE;
                    strcpy(strValue, "");
                    sizeStrValue = 0;
                    readState = RIO_READ_VALUE;
                }
                // Next is the end of a subField
                else if (currentChar == '(')
                {
                    if (readState == RIO_READ_NAME)
                        strcpy(currentField->name, strValue);
                    else if (readState == RIO_READ_VALUE)
                        strcpy(currentField->stringValue, strValue);
                    else
                        error = RIO_ERROR_FILE;
                    strcpy(strValue, "");
                    sizeStrValue = 0;
                    error = loadFieldsFromFile(file, currentField); // Get back after a ")" char, an EOF char or an error
                    readState = RIO_READ_NAME;
                    subFieldIndex++;
                    if (subFieldIndex < rootField->nbSubFields)
                        currentField = rootField->subFields[subFieldIndex];
                    else
                    {
                        currentField = NULL;
                        readState = RIO_READ_STOP;
                    }
                }
                // Next is the end of the field
                else if (currentChar == ';')
                {
                    if (readState == RIO_READ_NAME)
                        strcpy(currentField->name, strValue);
                    else if (readState == RIO_READ_VALUE)
                    {
                        strcpy(currentField->stringValue, strValue);
                        interpretFieldValue(currentField);
                    }
                    else
                        error = RIO_ERROR_FILE;
                    strcpy(strValue, "");
                    sizeStrValue = 0;
                    readState = RIO_READ_NAME;
                    // Don't increment the subField index if none has been read.
                    if (currentField->name[0] != '\0')
                    {
                        subFieldIndex++;
                        if (subFieldIndex < rootField->nbSubFields)
                            currentField = rootField->subFields[subFieldIndex];
                        else
                        {
                            currentField = NULL;
                            readState = RIO_READ_STOP;
                        }
                    }
                }
                // Add the char to the buffer string if it's not a char to ignore
                else if (currentChar != '\n' && 
                    currentChar != '\r' && 
                    currentChar != '\t' && 
                    currentChar != '\"' && 
                    currentChar != ' ' && 
                    currentChar != EOF)
                {
                    if (sizeStrValue < 256)
                    {
                        strChar[0] = currentChar;
                        sizeStrValue++;
                        strcat(strValue, strChar);
                    }
                }
            // We stop at the end of the file, if an error has been met, after an "end of subField" character or if we found more subField than planed
            } while (currentChar != EOF && error == RIO_ERROR_NONE && currentChar != ')');
        }
	}
	else
		error = RIO_ERROR_READ;
	return error;
}

int countFieldInFile(FILE* file)
{
    long int currentPos = ftell(file);

    // +1 each time a "(" is met, -1 each time a ")" is met
    int fieldDepth = 0;
    int nbSubField = 0;
    bool justFoundSubField = false;

    char currentChar;
    do
    {
        currentChar = fgetc(file);
        // Change depth
        if (currentChar == '(')
            fieldDepth++;
        if (currentChar == ')')
            fieldDepth--;

        // Only count had subField if we are at the correct fieldDepth and if the last valid character wasn't a subField
        if ((currentChar == ';' || currentChar == ')') && fieldDepth == 0 && !justFoundSubField)
        {
            nbSubField++;
            justFoundSubField = true;
        }

        // a new field has begin
        if (justFoundSubField &&
            currentChar != '\n' &&
            currentChar != '\r' && 
            currentChar != '\t' &&
            currentChar != '\"' &&
            currentChar != ' ' &&
            currentChar != ';' &&
            currentChar != ')')
            justFoundSubField = false;

    // We stop once we reach the end of the file or once we got to low on depth
    } while (currentChar != EOF && fieldDepth >= 0);
    fseek(file, currentPos, SEEK_SET);

    return nbSubField;
}

void printField(RIO_VarField* rootField, int offset)
{
    if (rootField != NULL)
    {
        printf("%p:", rootField);
        int i;
        for (i = 0; i < offset; i++)
            printf("\t");
        char type[50] = "";
        switch(rootField->type)
        {
        	case RIO_STRING_TYPE:
        		strcpy(type, " STRING");
        		break;
        	case RIO_INT_TYPE:
        		strcpy(type, "    INT");
        		break;
        	case RIO_FLOAT_TYPE:
        		strcpy(type, "  FLOAT");
        		break;
        	case RIO_BOOL_TYPE:
        		strcpy(type, "   BOOL");
        		break;
        	case RIO_FIELD_TYPE:
        		strcpy(type, "  FIELD");
        		break;
        	default:
        		strcpy(type, "UNKNOWN");
        		break;
        }
        printf("%s%s%s %s = %s%s%s\n",
        	BOLDRED_COLOR_TER, type, RESET_COLOR_TER,
        	rootField->name,
        	CYAN_COLOR_TER, rootField->stringValue, RESET_COLOR_TER);
        if (rootField->hasSubFields)
            for (i = 0; i < rootField->nbSubFields; i++)
                printField(rootField->subFields[i],offset+1);
    }
}

RIO_VarField* findField(const char* fieldTree, RIO_VarField* rootField)
{
    RIO_VarField* foundField = NULL;
    if (rootField != NULL)
    {
        // Separate the current field from the tree
        int i = 0;
        char currentFieldStr[256];
        strcpy(currentFieldStr, fieldTree);
        char* nextFieldsStr = NULL;
        while (currentFieldStr[i] != '\0')
        {
            if (currentFieldStr[i] == '/')
            {
                currentFieldStr[i] = '\0'; // Indicate the end of the current field
                nextFieldsStr = currentFieldStr + i + 1; // Indicate the beginning of the nextField
                break;
            }
            i++;
        }

        // Separate the name of the current field from the value
        i = 0;
        char* name = currentFieldStr;
        char* value = NULL;
        while (name[i] != '\0')
        {
            if (name[i] == '=')
            {
                name[i] = '\0'; // Indicate the end of the current field
                value = name + i + 1; // Indicate the beginning of the values
                break;
            }
            i++;
        }

        // Count all the fields that have the correct name and value
        int nbCorrectField = 0;
        for(i = 0; i < rootField->nbSubFields; i++)
        {
            if (strcmp(rootField->subFields[i]->name, name) == 0 &&
                (value == NULL || value[0] == '\0' || strcmp(rootField->subFields[i]->stringValue, value) == 0)) // NULL value means any is okay
                nbCorrectField++;
        }

        // Keep in memory all valid fields
        RIO_VarField* validFields[nbCorrectField];
        int indexValidFields = 0;
        for(i = 0; i < rootField->nbSubFields; i++)
        {
            if (strcmp(rootField->subFields[i]->name, name) == 0 &&
                (value == NULL || value[0] == '\0' || strcmp(rootField->subFields[i]->stringValue, value) == 0)) // NULL value means any is okay
            {
                 // If no subFields need to be searched, we save the current one
                if (nextFieldsStr == NULL || nextFieldsStr[0] == '\0')
                    validFields[indexValidFields] = rootField->subFields[i];
                // Otherwise, we do further research
                else
                    validFields[indexValidFields] = findField(nextFieldsStr, rootField->subFields[i]);
                indexValidFields++;
            }
        }

        // Find the first one different than NULL, it's our result
        for(indexValidFields = 0; indexValidFields < nbCorrectField; i++)
        {
            if (validFields[indexValidFields] != NULL)
            {
                foundField = validFields[indexValidFields];
                break;
            }
        }
    }
    return foundField;
}

void interpretFieldValue(RIO_VarField* field)
{
    if (field != NULL)
    {
        if (field->name != NULL && field->stringValue != NULL)
        {
            // Field Type
            if (field->hasSubFields || field->nbSubFields > 0 || field->subFields != NULL)
            {
                field->type = RIO_FIELD_TYPE;
                return;
            }

            // Bool type
            if (strcmp(field->stringValue, "FALSE") == 0)
            {
                field->boolValue = false;
                field->type = RIO_BOOL_TYPE;
                return;
            }
            if (strcmp(field->stringValue, "TRUE") == 0)
            {
                field->boolValue = true;
                field->type = RIO_BOOL_TYPE;
                return;
            }

            // Number Type
            if (isStrictlyComposedOf(field->stringValue, "0123456789.-+"))
            {
                bool couldBeAFloat = (strchr(field->stringValue, '.') != NULL);
                if (couldBeAFloat)
                {
                    // Float Type
                    float floatValue;
                    if (sscanf(field->stringValue, "%f", &floatValue) != 0)
                    {
                        field->floatValue = floatValue;
                        field->intValue = (int) floatValue;
                        field->type = RIO_FLOAT_TYPE;
                        return;
                    }
                }
                else
                {
                    // Int Type
                    int intValue;
                    if (sscanf(field->stringValue, "%d", &intValue) != 0)
                    {
                        field->floatValue = (float) intValue;
                        field->intValue = intValue;
                        field->type = RIO_INT_TYPE;
                        return;
                    }
                }
            }
            
            // String Type
            field->type = RIO_STRING_TYPE;
        }
    }
}

bool isStrictlyComposedOf(const char* string, const char* chars)
{
	bool stringValid = true;
	int stringIndex = 0;
	while (string[stringIndex] != '\0' && stringValid)
	{
		int charIndex = 0;
		while (chars[charIndex] != '\0')
		{
			stringValid = false;
			if (string[stringIndex] == chars[charIndex])
			{
				stringValid = true;
				break;
			}
			charIndex++;
		}
		stringIndex++;
	}
	return stringValid;
}


