#include "rioDisplay.h"

RIO_Camera* initCamera(SDL_Rect* cameraView, float windowWidth, float windowHeight)
{
	// We allocate the memory to store the structure
	RIO_Camera* camera = (RIO_Camera*) malloc(sizeof(RIO_Camera));
	// We set all the variables of the structure
	camera->x = cameraView->x;
	camera->y = cameraView->y;
	camera->width = cameraView->w;
	camera->height = cameraView->h;
	camera->windowWidth = windowWidth;
	camera->windowHeight = windowHeight;
	// We calculate the other variables
	camera->xScale = camera->windowWidth/cameraView->w;
	camera->yScale = camera->windowHeight/cameraView->h;
	camera->xOffset = cameraView->w/2;
	camera->yOffset = cameraView->h/2;
	// Then, we return the structure
	return camera;
}

RIO_ListCamera* initListCamera(int nbCamera, float xScale, float yScale, float windowWidth, float windowHeight)
{
	RIO_ListCamera* listCamera = NULL;
	if (1 <= nbCamera && nbCamera <= 4  && xScale > 0 && yScale > 0 && windowWidth > 0 && windowHeight > 0)
	{
		listCamera = (RIO_ListCamera*) malloc(sizeof(RIO_ListCamera));
		listCamera->cameras = (RIO_Camera**) malloc(sizeof(RIO_Camera*) * nbCamera);
		listCamera->size = nbCamera;
		listCamera->xScale = xScale;
		listCamera->yScale = yScale;
		SDL_Rect cameraView = {0, 0, windowWidth*xScale, windowHeight*yScale};
		int i;
		for (i = 0; i < nbCamera; i++)
		{
			listCamera->cameras[i] = initCamera(&cameraView, windowWidth, windowHeight);
		}
		updateCamerasDimmension(listCamera, xScale, yScale, windowWidth, windowHeight);
	}
	return listCamera;
}

void destructCamera(RIO_Camera** camera)
{
	// If the pointer exists
 	if (camera != NULL)
 	{
 		// If what he points for exists
 		if (*camera != NULL)
 		{
 			// Free the camera from the memory
 			free(*camera);
 			*camera = NULL;
 		}
 	}
}

void destructListCamera(RIO_ListCamera** listCamera)
{
	if (listCamera != NULL)
 	{
 		// If what he points for exists
 		if (*listCamera != NULL)
 		{
 			int i;
			for (i = 0; i < (*listCamera)->size; i++)
			{
				destructCamera(&(*listCamera)->cameras[i]);
	 		}
	 		free((*listCamera)->cameras);
	 		free(*listCamera);
	 		*listCamera = NULL;
	 	}
 	}
}

void updateCamerasDimmension(RIO_ListCamera* listCamera, float xScale, float yScale, float windowWidth, float windowHeight)
{
	if (listCamera != NULL && xScale > 0 && yScale > 0 && windowWidth > 0 && windowHeight > 0)
	{
		listCamera->xScale = xScale;
		listCamera->yScale = yScale;
		SDL_Rect cameraView[listCamera->size];
		switch(listCamera->size)
		{
			case 1:
				cameraView[0] = (SDL_Rect) {0, 0, windowWidth/xScale, windowHeight/yScale};
				break;
			case 2:
				cameraView[0] = (SDL_Rect) {0, 0, windowWidth/xScale/2, windowHeight/yScale};
				cameraView[1] = (SDL_Rect) {0, 0, windowWidth/xScale/2, windowHeight/yScale};
				break;
			case 3:
				cameraView[0] = (SDL_Rect) {0, 0, windowWidth/xScale/2, windowHeight/yScale/2};
				cameraView[1] = (SDL_Rect) {0, 0, windowWidth/xScale/2, windowHeight/yScale/2};
				cameraView[2] = (SDL_Rect) {0, 0, windowWidth/xScale, windowHeight/yScale/2};
				break;
			case 4:
				cameraView[0] = (SDL_Rect) {0, 0, windowWidth/xScale/2, windowHeight/yScale/2};
				cameraView[1] = (SDL_Rect) {0, 0, windowWidth/xScale/2, windowHeight/yScale/2};
				cameraView[2] = (SDL_Rect) {0, 0, windowWidth/xScale/2, windowHeight/yScale/2};
				cameraView[3] = (SDL_Rect) {0, 0, windowWidth/xScale/2, windowHeight/yScale/2};
				break;
		}

		int i;
		for (i = 0; i < listCamera->size; i++)
		{
			updateCameraDimmension(
				listCamera->cameras[i],
				cameraView[i].w,
				cameraView[i].h,
				cameraView[i].w*xScale,
				cameraView[i].h*yScale
				);
		}
	}
}

void updateCameraDimmension(RIO_Camera* camera, float newWidth, float newHeight, float windowWidth, float windowHeight)
{
	// We update the height and width of the camera view and of the window
	camera->width = newWidth;
	camera->height = newHeight;
	camera->windowWidth = windowWidth;
	camera->windowHeight = windowHeight;
	// We calculate the other variables
	camera->xScale = windowWidth/newWidth;
	camera->yScale = windowHeight/newHeight;
	camera->xOffset = newWidth/2;
	camera->yOffset = newHeight/2;
}

void updateTextureRectWithCamera(RIO_Camera* camera, SDL_Rect* tileRect)
{
	// If their is a camera and if the rectangle exists
	if (camera != NULL && tileRect != NULL)
	{
		// We update the coordinate of rectangle with the camera as an offset
		tileRect->x = round((tileRect->x - camera->x + camera->xOffset) * camera->xScale);
		tileRect->y = round((tileRect->y - camera->y + camera->yOffset) * camera->yScale);
		tileRect->w = ceil(tileRect->w * camera->xScale);
		tileRect->h = ceil(tileRect->h * camera->yScale);
	}
}

void moveCameraToPoint(RIO_Camera* camera, int x, int y, float movementRatio)
{
	// If the movement ratio is between 0 and 1
	if (0 <= movementRatio && movementRatio <= 1)
	{
		// We update the camera coordinate
		camera->x += (x - camera->x)*movementRatio;
		camera->y += (y - camera->y)*movementRatio;
	}
}

void setViewPortScreen(RIO_ListCamera* listCamera, int idScreen, SDL_Renderer* renderer)
{
	SDL_Rect screenRect = {0, 0, listCamera->cameras[idScreen]->windowWidth, listCamera->cameras[idScreen]->windowHeight};
	// Bottom screens
	if (idScreen >= 2)
		screenRect.y = listCamera->cameras[idScreen]->windowHeight;
	// Screens on the right
	if (idScreen%2 == 1)
		screenRect.x = listCamera->cameras[idScreen]->windowWidth;

	SDL_RenderSetViewport(renderer, &screenRect);
}

void getCameraOffsetWhenCliped(RIO_Camera* camera, int idScreen, int nbScreen, float* offset)
{

	offset[0] = 0;
	offset[1] = 0;
	if (camera != NULL && offset != NULL && nbScreen > 1)
	{
		if (idScreen%2 == 0)
			offset[0] = -camera->width/2;
		else if (idScreen%2 == 1)
			offset[0] = camera->width/2;
		switch(nbScreen)
		{
			case 3:
			case 4:
				if (idScreen < 2)
					offset[1] = -camera->height/2;
				else if (idScreen >= 2)
					offset[1] = camera->height/2;
				break;
		}
	}
}

void getCameraShapeTwoPlayer(RIO_Shape** shapes, RIO_Point* player1, RIO_Point* player2, int windowWidth, int windowHeight)
{
	if (player1 != NULL && player2 != NULL)
	{
		if (shapes[0] == NULL)
			shapes[0] = initShape(4);
		if (shapes[1] == NULL)
			shapes[1] = initShape(4);
		RIO_Point centerPoint = {windowWidth/2, windowHeight/2};

		float angle = atan2(player2->y - player1->y, player2->x - player1->x);
		angle += M_PI/2;
		if (angle < 0)
			angle += 2*M_PI;

		RIO_Point pointOnLine = {cos(angle) + centerPoint.x, sin(angle) + centerPoint.y};
		if (pointOnLine.x != centerPoint.x)
		{
			// y = ax + b
			float a = (centerPoint.y - pointOnLine.y)/(centerPoint.x - pointOnLine.x);
			float b = centerPoint.y - a*centerPoint.x;

			// The separation line split the screen horizontaly
			if (fabsf(cos(angle))*windowHeight > fabsf(sin(angle))*windowWidth)
			{
				shapes[0]->listPoint[0] = (RIO_Point) {0, b};
				shapes[0]->listPoint[3] = (RIO_Point) {windowWidth-1, a * windowWidth + b};
				shapes[1]->listPoint[0] = (RIO_Point) {0, b};
				shapes[1]->listPoint[3] = (RIO_Point) {windowWidth-1, a * windowWidth + b};
				// The player 1 is bellow the player 2
				if (player1->y > player2->y)
				{
					shapes[0]->listPoint[1] = (RIO_Point) {0, windowHeight-1};
					shapes[0]->listPoint[2] = (RIO_Point) {windowWidth-1, windowHeight-1};
					shapes[1]->listPoint[1] = (RIO_Point) {0, 0};
					shapes[1]->listPoint[2] = (RIO_Point) {windowWidth-1, 0};
				}
				else
				{
					shapes[0]->listPoint[1] = (RIO_Point) {0, 0};
					shapes[0]->listPoint[2] = (RIO_Point) {windowWidth-1, 0};
					shapes[1]->listPoint[1] = (RIO_Point) {0, windowHeight-1};
					shapes[1]->listPoint[2] = (RIO_Point) {windowWidth-1, windowHeight-1};
				}
			}
			else
			{
				shapes[0]->listPoint[0] = (RIO_Point) {-b/a, 0};
				shapes[0]->listPoint[3] = (RIO_Point) {windowHeight/a -b/a, windowHeight-1};
				shapes[1]->listPoint[0] = (RIO_Point) {-b/a, 0};
				shapes[1]->listPoint[3] = (RIO_Point) {windowHeight/a -b/a, windowHeight-1};
				// The player 1 is to the right of the player 2
				if (player1->x > player2->x)
				{
					shapes[0]->listPoint[1] = (RIO_Point) {windowWidth-1, 0};
					shapes[0]->listPoint[2] = (RIO_Point) {windowWidth-1, windowHeight-1};
					shapes[1]->listPoint[1] = (RIO_Point) {0, 0};
					shapes[1]->listPoint[2] = (RIO_Point) {0, windowHeight-1};
				}
				else
				{
					shapes[0]->listPoint[1] = (RIO_Point) {0, 0};
					shapes[0]->listPoint[2] = (RIO_Point) {0, windowHeight-1};
					shapes[1]->listPoint[1] = (RIO_Point) {windowWidth-1, 0};
					shapes[1]->listPoint[2] = (RIO_Point) {windowWidth-1, windowHeight-1};
				}
			}
		}
		else
		{
			shapes[0]->listPoint[0] = (RIO_Point) {windowWidth/2, 0};
			shapes[0]->listPoint[3] = (RIO_Point) {windowWidth/2, windowHeight-1};
			shapes[1]->listPoint[0] = (RIO_Point) {windowWidth/2, 0};
			shapes[1]->listPoint[3] = (RIO_Point) {windowWidth/2, windowHeight-1};
			// The player 1 is to the right of the player 2
			if (player1->x > player2->x)
			{
				shapes[0]->listPoint[1] = (RIO_Point) {windowWidth-1, 0};
				shapes[0]->listPoint[2] = (RIO_Point) {windowWidth-1, windowHeight-1};
				shapes[1]->listPoint[1] = (RIO_Point) {0, 0};
				shapes[1]->listPoint[2] = (RIO_Point) {0, windowHeight-1};
			}
			else
			{
				shapes[0]->listPoint[1] = (RIO_Point) {0, 0};
				shapes[0]->listPoint[2] = (RIO_Point) {0, windowHeight-1};
				shapes[1]->listPoint[1] = (RIO_Point) {windowWidth-1, 0};
				shapes[1]->listPoint[2] = (RIO_Point) {windowWidth-1, windowHeight-1};
			}
		}
	}
}

void showShape(SDL_Renderer* renderer, RIO_Shape* shape)
{
	if (renderer != NULL && shape != NULL)
	{
		int i;
		for (i = 0; i < shape->nbPoint; i++)
		{
			SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255);
			SDL_RenderDrawLine(
				renderer,
				shape->listPoint[i].x,
				shape->listPoint[i].y,
				shape->listPoint[(i+1)%shape->nbPoint].x,
				shape->listPoint[(i+1)%shape->nbPoint].y);
		}
	}
}

void toggleFullScreen(RIO_ListCamera* listCamera, int originalScreenWidth, int originalScreenHeight, SDL_Renderer* renderer, SDL_Window* window)
{
	if (renderer != NULL && window != NULL)
	{
		// Get if the screen is full or not
		bool IsFullscreen = SDL_GetWindowFlags(window) & SDL_WINDOW_FULLSCREEN_DESKTOP;
		// get the actual width and height of the screen
		int currentWidth = originalScreenWidth;
		int currentHeight = originalScreenHeight;
		SDL_GetWindowSize(window, &currentWidth, &currentHeight);
		// Prepare the scales for later
		float xScale = currentWidth;
		float yScale = currentHeight;

		// Change the screen mode and update his current size
		if (IsFullscreen)
		{
			SDL_SetWindowFullscreen(window, 0);
			currentWidth = originalScreenWidth;
			currentHeight = originalScreenHeight;
		}
		else
		{
			SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
			SDL_GetWindowSize(window, &currentWidth, &currentHeight);
		}

		// Resize the renderer
		SDL_RenderSetLogicalSize(renderer, currentWidth, currentHeight);
		// Update the scale
		xScale = currentWidth/xScale * listCamera->xScale;
		yScale = currentHeight/yScale * listCamera->yScale;
		// Update the cameras
		updateCamerasDimmension(listCamera, xScale, yScale, currentWidth, currentHeight);
	}
}

