
// Header file
#include "rioPathfinding.h"
#include "rioKeyboardInput.h"
#include "rioType.h"
#include "rioFile.h"

// Use to set the type of an entity
typedef enum RIO_TypeEntity {NOTYPE = 0, PLAYER, ENEMY, NEUTRAL, NB_TYPES} RIO_TypeEntity;
extern char* TYPE_ENTITY_STRING[NB_TYPES];

// Use to set what the entity is doing and their priority
// Only a state with a priority equal or higher could interrupt an other state
typedef enum RIO_StateEntity {IDLE = 0, MOVING, ATTACKING, KNOCKBACK, DEAD, NB_STATES} RIO_StateEntity;
extern char* STATE_ENTITY_STRING[NB_STATES];
#define STATE_PRIORITIES {0, 0, 1, 2, 3}

// use to set the direction the entity is going
typedef enum RIO_DirectionEntity {UP = 0, RIGHT, DOWN, LEFT, NB_DIRECTIONS} RIO_DirectionEntity;
extern char* DIRECTION_ENTITY_STRING[NB_DIRECTIONS];

// Use to set the type of path
// ONE_WAY 			-> follow the path and stop at the end
// BACK_AND_FORTH 	-> follow the path and back track at the end
// LOOP				-> follow the path and go back to the beginning at the end
typedef enum RIO_TypePath {ONE_WAY = 0, BACK_AND_FORTH, LOOP} RIO_TypePath;

#define GLOBAL_MAX_SPEED 10 // The maximum speed possible for all entities
// The direction vector of the entity needs to have at least one of his value superior to the threshold to be abble to move
#define MOVEMENT_THRESHOLD 0.3

// This structure is use to create and control entities
// An entity is a structure which can move and is animated according to this
typedef struct RIO_Entity
{
	float maxHealth;							// The maximum health of the entity
	float currentHealth;						// The current health of the entity
	float damage;								// The damage the entity does when attacking

	float x;									// The x coordinate of the entity
	float y;									// The y coordinate of the entity
	float speedVector[2]; 						// The current speed of the entity at wich it'll move
	float maxSpeed;								// The maximum speed the entity can have by itself (without collision) in pixel per frame
	float direction[2];							// The direction vector the entity will try to go
												// 	this is a vector with an x and y component. This will be updated by the inputs
	float accelerationFactor;					// The amount of the maxSpeed the entity loose or gain each frame while not moving

	float repelSpeed;							// The speed at wich the entities getting into the collision box will be repel
	bool isSolid;								// To know if the entity is solid or not. If it is, an entity can not go throught anymore
	bool isImmovable;							// To know if the entity is immovable or not. If it is, the entity cannot be moved by an other entity
	SDL_Rect collisionBox; 						// The collision box of the entity. It is relative to the top left corner of the entity
	SDL_Rect* attackCollisionBox;				// The collision boxes of the attacks. attackCollisionBox[indexAnimation]
	bool attackCollisionIsActive;				// To know if the collision box is active or not
	bool friendlyFire;							// Define if the entity can apply friendly fire or not

	int indexAnimation;							// The type of animation the entity is playing (based on the tileset. The height index)
	int frameAnimation;							// The animation frame at wich the entity is currently on
	float speedAnimation;						// the time between each animation frame of the entity
	int animationClock;							// To keep track on how many frame has passed since the last animation frame

	int* offsets[NB_STATES][2];					// To store the x and y offsets depending on the state of the entity
												// 	offsets[state][offset axes (0 = x, 1 = y)][indexAnimation]
	RIO_TileSetEntity* tileSet[NB_STATES];		// The tileset linked to the entity (the size of a tile is use as collision box for now)

	RIO_TypeEntity type;						// The type of the entity
	RIO_StateEntity state;						// Define what the entity is doing
	bool stateIsLock;							// To know if a state could be changed by a less higher priority state

	node* pathToFollow;							// A path loaded into an entity that he will try to follow
	node* nextNodeInPath;						// To keep track at which node we are on the path
	RIO_TypePath pathType;						// To know the type of the path

}RIO_Entity;

// Use to store a list of entities
typedef struct RIO_ListEntity
{
	RIO_Entity** entities;		// The entities stored in the list
	int size;					// The size of the list
}RIO_ListEntity;

//////////////////////////////////////////
// Initialisation and creation function //	use to create, destruct and edit entities
//////////////////////////////////////////

/* @function
 * 		Use to initialize an entity structure. The default collision box of the entity is set around his tile
 *
 * @param
 *		tileSet : 				the tileset use by the entity when it is shown. Put NULL if their is none
 *								Because a single tileSet could be use on multiple entity it will not be free with destructEntity(),
 *								be sure to have an extern variable to store it so it can be free proprely with destructTileSet()
 *		x : 					the x coordinate of the entity (colomn)
 *		y : 					the y coordinate of the entity (row).
 *								(0, 0) is the top left corner
 *		speedAnimation : 		the time between each animation frame of the entity
 *		maxSpeed :				the maximum speed the entity can go to
 *		repelSpeed : 			the speed at wich the entity will repel others if they are in his collision box
 *		accelerationFactor : 	the amont of the max speed the entity can loose or gain at each frame if it is not moving
 *								0 means that the entity don't slow down
 *								1 means that it stop as fast as possible
 *		type : 					the type of the entity
 *
 * @return
 * 		Returns a RIO_Entity structure
 */
RIO_Entity* initEntity(RIO_TileSetEntity* idleTileSet, float x, float y, float speedAnimation, float maxSpeed, float repelSpeed, float accelerationFactor, RIO_TypeEntity type);

/* @function
 * 		Use to initialize a list of entities. Each entity in the list are set to NULL
 *
 * @param
 *		size : 	the size of the list
 *
 * @return
 * 		Returns a RIO_ListEntity structure
 */
RIO_ListEntity* initListEntity(int size);

/* @function
 * 		Use to free an entity from the memory. Warning, this function will not free the tileset from the memory
 *
 * @param
 *		entity : 	the entity we want to free from the memory
 */
void destructEntity(RIO_Entity** entity);

/* @function
 * 		Use to free a list of entities, this function will not free the tilesets of the entities from the memory
 *
 * @param
 *		listEntity : 	the list of entities we want to free from the memory
 */
void destructListEntity(RIO_ListEntity** listEntity);

/* @function
 * 		Use to update the collision box of an entity
 *
 * @param
 *		entity : 	the entity we want to change the collision box
 *		xOffset : 	the x offset of the collision box, relative to the top left corner of the entity
 *		yOffset : 	the y offset of the collision box, relative to the top left corner of the entity
 *		width : 	width of the collision box
 *		height : 	height of the collision box
 */
void updateCollisionBox(RIO_Entity* entity, float xOffset, float yOffset, float width, float height);

/* @function
 * 		Use to load an entity from a file
 *
 * @param
 *		entity : 				the entity where the data will be load.
 *		pathEntity : 			the path of the file where the entity info is stored
 *		renderer : 				a renderer use to extract the tileset textures
 *		transparencyColor : 	the color use as transparency in the tileset textures
 *
 * @return
 *		return an error ID depending on what append
 */
int loadEntity(RIO_Entity** entity, char* pathEntityInfo, SDL_Renderer* renderer, SDL_Color transparencyColor);

////////////////////
// Usage function //	function used to manipulate the entities
////////////////////

/* @function
 * 		Use to show an entity in a renderer
 *
 * @param
 *		entity : 	the entity we want to show
 *		renderer : 	the renderer use to show the entity
 *		camera : 	the camera that will show the environment. If their is none, put NULL
 */
void showEntity(RIO_Entity* entity, SDL_Renderer* renderer, RIO_Camera* camera);

/* @function
 * 		Use to show all the entities in a renderer
 *
 * @param
 *		listEntity : 	the entities we want to show
 *		renderer : 		the renderer use to show the entity
 *		camera : 		the camera that will show the environment. If their is none, put NULL
 */
void showEntities(RIO_ListEntity* listEntity, SDL_Renderer* renderer, RIO_Camera* camera);

/* @function
 * 		Use to move the different cameras to their respective target
 *		This fonction include camera snaping (they all create one solid screen) with up to 2 players
 *
 * @param
 *		listCamera : 		the camera we want to move
 *		movementRatio : 	defines the speed at wich the camera will move.
 *							It is a ratio between 0 and 1. 0 it doesn't move, 1 it moves instantaneously
 *		player1 : 			The first player the cameras need to follow. Must not be NULL
 *		player2 : 			The second player the cameras need to follow. Put NULL if their is none
 *		player3 : 			The third player the cameras need to follow. Put NULL if their is none
 *		player4 : 			The fourth player the cameras need to follow. Put NULL if their is none
 */
void moveMultiPlayerCameras(RIO_ListCamera* listCamera, float movementRatio, RIO_Entity* player1, RIO_Entity* player2, RIO_Entity* player3, RIO_Entity* player4);

/* @function
 * 		Use to update the speed vector and the position of all entities, according to its original speed vector
 *		This function will also provide the entities to not go on solid tile
 *
 * @param
 *		listEntity : 		the entities which we want to update the movement
 *		map : 				the map we want to check the collision from
 */
void updatePositionEntities(RIO_ListEntity* listEntity, RIO_Map* map);

/* @function
 * 		Use to update the animation of an entity
 *
 * @param
 *		entity : 			the entity which we want to update the animation
 *		framePerSecond : 	the fps at which the game is currently running
 */
void updateAnimationEntity(RIO_Entity* entity, int framePerSecond);

/* @function
 * 		Use to update the animation of a all entities
 *
 * @param
 *		entities : 			the entities which we want to update the animation
 *		framePerSecond : 	the fps at which the game is currently running
 */
void updateAnimationEntities(RIO_ListEntity* listEntity, int framePerSecond);

/* @function
 * 		Use to update the state of an entity according to the keyboard input
 *
 * @param
 *		entity : 			the entity which we want to update
 *		keyboardInput : 	the inportant keyboard inputs
 *		player : 			to know which set of keyboard mapping will be used (each player have a set of key)
 */
void updateStateEntityWithInputs(RIO_Entity* entity, RIO_KeyboardInput* keyboardInput, RIO_TypePlayer player);

/* @function
 * 		Use to update the speed and direction of an entity based on the other entities
 *
 * @param
 *		entity : 		the entity which we want to update
 *		listEntity : 	the list of all the entities present on the map
 */
void updateEntityCollisionWithEntities(RIO_Entity* entity, RIO_ListEntity* listEntity);

/* @function
 * 		Use to update the speed and direction of all entities based on the other entities
 *
 * @param
 *		listEntity : 	the list of all the entities present on the map
 */
void updateEntitiesCollisionWithEntities(RIO_ListEntity* listEntity);

/////////////////////////
// Secondary functions //	functions that should not be called by the user
/////////////////////////

/* @function
 * 		Use to check if a position is colliding with an object in the map
 *
 * @param
 *		map : 				the map we'll use as a reference for the collisions
 *		collisionBox :		the colision box we want to check the collision from
 *		overlapVector : 	a 2D vector which will be update if their is a collision
 *							the first value indicate the overlap on the x axis
 *							the second value indicate the overlap on the y axis
 *
 * @return
 *		return true if their is a collision, false otherwise
 */
bool checkBoxColideWithMap(RIO_Map* map, SDL_Rect* collisionBox, int* overlapVector);

/* @function
 * 		Use to check if a position is colliding with a solid entity in the given list
 *
 * @param
 *		listEntity : 		the list of entities
 *		checkedEntity : 	the entity that is currently being checked. This is use so a collision with himself is not detected
 *							put NULL if their is none
 *		collisionBox :		the colision box we want to check the collision from
 *		overlapVector : 	a 2D vector which will be update if their is a collision
 *							the first value indicate the overlap on the x axis
 *							the second value indicate the overlap on the y axis
 *		isSolid : 			if set to true, the function will only check for collisions with solid entities.
 *							if set to false, the function will only checjk for collisions with non-solid entities.
 *
 * @return
 *		return true if their is a collision, false otherwise
 */
bool checkBoxColideWithEntities(RIO_ListEntity* listEntity, RIO_Entity* checkedEntity, SDL_Rect* collisionBox, int* overlapVector, bool isSolid);

/* @function
 * 		Crop the speed vector according to the solid tiles of the map and the solid entities,
 * 		then, update the position of the entity.
 *		This is the last step to make an entity move.
 *
 * @param
 *		map : 			the map we'll use as a reference for the collisions
 *		listEntities : 	the list of entities we'll use as a reference for the collisions
 *		entity : 		the entity from wich we will base on the speed calculation
 *						it is based on the speed, the direction, the coordinate and the collision box of the entity
 *
 */
void applySpeedVector(RIO_Map* map, RIO_ListEntity* listEntity, RIO_Entity* entity);

/* @function
 * 		Use to change the speed vector of a solid entity according to their collisions with other entities (being pushed).
 * 		This fonction does nothing if the entity is flaged as immovable
 * 		If an entity is solid, this is the second step to make it move
 *
 * @param
 *		listEntities : 	the list of entities we'll use as a reference for the collisions
 *		entity : 		the entity which will be updated
 */
void updateSpeedVectorOfSolidEntity(RIO_ListEntity* listEntity, RIO_Entity* entity);

/* @function
 * 		Use to change the vectors of entities that collide with solid, immovable entities
 *
 * @param
 *		listEntities : 	the list of entities we'll use as a reference for the collisions
 *		entity : 		the immovable entity that will move other entities around
 */
void updateImmovableEntityCollisionWithEntities(RIO_ListEntity* listEntity, RIO_Entity* entity);

/* @function
 * 		Use to change the speed vector of an entity according to it's own acceleration factor.
 * 		This function does not take into account collision
 * 		This is the first step to make an entity move
 *
 * @param
 *		entity : 		the entity from wich we will base on the speed calculation
 *						it is based on the speed, the direction, the coordinate and the collision box of the entity
 */
void updateSpeedVectorFromAcceleration(RIO_Entity* entity);

/* @function
 * 		Reduce the health of an entity by a fixed amount
 *
 * @param
 *		entity : 	the entity that will take damage
 *		damage : 	the damage inflicted to the entity
 */
void applyDamageToEntity(RIO_Entity* entity, int damage);

/* @function
 * 		Try to change the state of an entity
 *
 * @param
 *		entity : 	the entity we want to change the state of
 *		newState : 	the new state the entity will have if everything is okay
 *
 * @return
 *		return true if the change of state could be done and has be done. Return false otherwise
 */
bool changeStateEntity(RIO_Entity* entity, RIO_StateEntity newState);

/* @function
 * 		get the center of an entity according to his collision box
 *
 * @param
 *		entity : 	the entity we want to get the center point of
 *		position : 	a list of two elements that will be updated with the center coordinate of the entity
 *
 */
void getCenterPosEntity(RIO_Entity* entity, float* position);

/* @function
 * 		get the center of a list of entity according to their collision box
 *
 * @param
 *		entities : 	the entity list of entity we want to get the center point of
 *		nbEntity : 	the number of entity
 *		position : 	a list of two elements that will be updated with the center coordinate of the entity
 *
 */
void getCenterPosEntities(RIO_Entity** entities, int nbEntity, float* position);

/* @function
 * 		converte a string to a RIO_StateEntity
 *
 * @param
 *		str : 	the string to converte
 *
 * @return
 *		return a RIO_StateEntity
 */
RIO_StateEntity aToStateEntity(char* str);

/* @function
 * 		converte a string to a RIO_TypeEntity
 *
 * @param
 *		str : 	the string to converte
 *
 * @return
 *		return a RIO_TypeEntity
 */
RIO_TypeEntity aToTypeEntity(char* str);

/* @function
 * 		converte a string to a RIO_TypePath
 *
 * @param
 *		str : 	the string to converte
 *
 * @return
 *		return a RIO_TypePath
 */
RIO_TypePath aToTypePath(char* str);

///////////////////////////
// Pathfinding functions //		Use to find path for the entities to navigate in the map
///////////////////////////

/* @function
 * 		Use to get the path from an entity to a specific coordinate which avoid the solid tiles in the map
 *
 * @param
 *		entity : 	the entity from which will contain the path and follow it
 *		x, y : 		the coordinate the entity will try to go to
 *		map : 		the map where the entity navigate
 */
void getCompleteEntityPathToCoord(RIO_Entity* entity, float x, float y, RIO_Map* map);

/* @function
 * 		Use to get the path from an entity to an other entity and avoid the solid tiles in the map
 *
 * @param
 *		entity : 		the entity from which will contain the path and follow it
 *		entityToGo :  	the entity to follow
 *		map : 			the map where the entity navigate
 */
void getCompleteEntityPathToEntity(RIO_Entity* entity, RIO_Entity* entityToGo, RIO_Map* map);

/////////////////////////
//	Movement functions //		Use to controle the movement of an entity
/////////////////////////

/* @function
 * 		update the directionVector of the entity according to the given angle
 *		it also update its state
 *
 * @param
 *		entity : 	the entity that will move
 *		angle : 	the direction the entity must go
 */
void moveEntityToDirection(RIO_Entity* entity, float angle);

/* @function
 * 		update the directionVector of the entity so it moves closer to the given point, until it is close enought
 *		it also update its state
 *
 * @param
 *		entity : 	the entity that will move
 *		x, y : 		the coordinate of the point we want the entity to move to
 *		dist : 		the distance from the point targeted. It's the definition of "close enought"
 *
 * @return
 *		return true if the entity arrived to the targeted point
 */
bool moveEntityToCoord(RIO_Entity* entity, float x, float y, float dist);

/* @function
 * 		update the directionVector of the entity so it moves away to the given point, until it is far enought
 *		it also update its state
 *
 * @param
 *		entity : 	the entity that will move
 *		x, y : 		the coordinate of the point we want the entity to move away from
 *		dist : 		the distance from the point targeted. It's the definition of "far enought". Put "-1" their is none
 *
 * @return
 *		return true if the entity moved away from the point enought
 */
bool moveEntityAwayFromCoord(RIO_Entity* entity, float x, float y, float dist);

/* @function
 * 		update the directionVector of the entity so it moves along his pathToFollow
 *		this function is to be used on a path which will not be updated regularly (guard shift for exemple)
 *		this function will change depending on the pathType of the entity
 *		it also update the state of the entity
 *
 * @param
 *		entity : 	the entity that will move
 *		dist : 		the distance from the path the entity is allowed to go
 *
 * @return
 *		return true if the entity arrived at destination
 */
bool followStaticPath(RIO_Entity* entity, float dist);

/* @function
 * 		update the directionVector of the entity so it moves along his pathToFollow
 *		this function is to be used on path which are updated regularly (a path to follow an entity for exemple),
 *		because of that, this function destroy certain parts of the path and is always concider as a ONE_WAY path
 *		it also update the state of the entity
 *
 * @param
 *		entity : 	the entity that will move
 *		dist : 		the distance from the end of the path at which the entity stops
 *
 * @return
 *		return true if the entity arrived at destination (ONE_WAY), complete a loop (LOOP) or turn away (BACK_AND_FORTH)
 */
bool followDynamicPath(RIO_Entity* entity, float dist);

//////////////////////////
//	Animation functions //	Use to controle the animation of an entity (used in the updateAnimation() function)
//////////////////////////

/* @function
 * 		increase the frame of the animation of the given entity based on the animation speed
 *
 * @param
 *		entity : 			the entity that will be animated
 *		framePerSecond : 	the fps at which the game is currently running
 */
void upFrameAnimationWithSpeed(RIO_Entity* entity, int framePerSecond);

/* @function
 * 		increase the frame of the animation of the given entity according to his animation speed until the end of the animation
 *
 * @param
 *		entity : 			the entity that will be animated
 *		framePerSecond : 	the fps at which the game is currently running
 *
 * @return
 *		return true if the animation has ended, false otherwise
 */
bool upFrameAnimationUntilEnd(RIO_Entity* entity, int framePerSecond);

/* @function
 * 		increase the frame of the animation of the given entity if the clock of the entity is at the given timing
 *
 * @param
 *		entity : 			the entity that will be animated
 *		timing : 			the timing at which the frame animation will be updated in seconds
 *		framePerSecond : 	the fps at which the game is currently running
 */
void upFrameAnimationAtTiming(RIO_Entity* entity, float timing, int framePerSecond);

/* @function
 * 		put the corresponding stateIsUnlock flag of an entity at true if the clock of the entity is at the given timing
 *
 * @param
 *		entity : 			the entity that will be updated
 *		timing : 			the timing at which the state will be ended in seconds
 *		framePerSecond : 	the fps at which the game is currently running
 */
void unlockStateAtTiming(RIO_Entity* entity, float timing, int framePerSecond);

/* @function
 * 		put the attackCollisionIsActive of the entity at true or false at the given timing
 *
 * @param
 *		entity : 			the entity that will be updated
 *		timing : 			the timing at which the state will be ended in seconds
 *		framePerSecond : 	the fps at which the game is currently running
 */
void switchAttackCollisionActivation(RIO_Entity* entity, float timing, int framePerSecond);

///////////////////////////////////
// Visualisation debug functions //
///////////////////////////////////

/* @function
 * 		Use to show an entity collision box in a renderer
 *
 * @param
 *		entity : 	the entity we want to show the collision box of
 *		renderer : 	the renderer use to show the entity
 *		camera : 	the camera that will show the environment
 */
void showCollisionBoxEntity(RIO_Entity* entity, SDL_Renderer* renderer, RIO_Camera* camera);

/* @function
 * 		Use to show all the collision box of the entities in a renderer
 *
 * @param
 *		listEntity : 	the list of entities we want to show the collision box of
 *		renderer : 		the renderer use to show the entity
 *		camera : 		the camera that will show the environment
 */
void showCollisionBoxEntities(RIO_ListEntity* listEntity, SDL_Renderer* renderer, RIO_Camera* camera);

/* @function
 * 		Use to show an entity attack collision box in a renderer when it's active
 *
 * @param
 *		entity : 	the entity we want to show the attack collision box of
 *		renderer : 	the renderer use to show the entity
 *		camera : 	the camera that will show the environment
 */
void showAttackCollisionBoxEntity(RIO_Entity* entity, SDL_Renderer* renderer, RIO_Camera* camera);

/* @function
 * 		Use to show the health of an entity
 *
 * @param
 *		entity : 	the entity we want to show the health
 *		renderer : 	the renderer use to show the entity
 *		camera : 	the camera that will show the environment
 */
void showHealthEntity(RIO_Entity* entity, SDL_Renderer* renderer, RIO_Camera* camera);

/* @function
 * 		Use to show the health of an entity
 *
 * @param
 *		listEntity : 	the list of entities we want to show the health
 *		type : 			the type of entity we want to show the health of. Put NONE if you want to show all the health bar.
 *		renderer : 		the renderer use to show the entity
 *		camera : 		the camera that will show the environment
 */
void showHealthEntities(RIO_ListEntity* listEntity, RIO_TypeEntity type, SDL_Renderer* renderer, RIO_Camera* camera);

//////////////////////////
// Conversion functions //
//////////////////////////

RIO_TypeEntity typeEntity_StringToEnum(const char* typeStr);
RIO_StateEntity stateEntity_StringToEnum(const char* stateStr);
RIO_DirectionEntity directionEntity_StringToEnum(const char* directionStr);

